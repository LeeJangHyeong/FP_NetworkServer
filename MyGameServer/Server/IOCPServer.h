#pragma once
#ifdef _WIN32
/*
	Windows IOCP를 사용하여 Accept Close Send Recv를 클래스.
*/

#include "MyGameServer.h"
#include "Server.h"
#include "DataType/Player.h"
#include "DataType/Overlapped.h"
#include "DataType/ObjectPool.h"
#include "DataType/Socket.h"
#include <WinSock2.h>
#include <string>
#include <tuple>
#include <thread>
#include <map>
#include <memory>

enum class ELogLevel;
class CRoomManager;
class CPlayerManager;
class CServerNetworkSystem;
class CTCPReceiveProcessor;
class CUDPReceiveProcessor;

using std::map;
using std::thread;

class CIOCPServer final : public IServer
{
public:
	static CIOCPServer* GetInstance();
	~CIOCPServer();

	// Interface of IServer
	virtual bool Run() override;
	virtual bool IsRun() override;
	virtual void Send(const std::shared_ptr<CPlayer>& player, const char* buf, const int& sendLen, bool isTCP) override;
	virtual void Close(CSocket* socket) override;
	virtual void SendTo(FSocketAddrIn* sockaddr, const char* buf, const int& sendLen) override;
	virtual void Shutdown() override;

	HANDLE hcp;

private:
	void PostSend(const std::shared_ptr<CPlayer>& player, const char* buf, const int& sendLen, bool isTCP);
	void PostUDPSend(FSocketAddrIn* sockaddr, const char* buf, const int& sendLen);
	void PostClose(CSocket* socket);
	void PostAccept();
	void PostRecv(FBuffuerableOverlapped* overlap, bool isTCP);

	static CIOCPServer* _instance;
	CIOCPServer();
	bool _bIsRun;

	CRoomManager* RoomManager;
	CPlayerManager* PlayerManager;
	CServerNetworkSystem* ServerNetworkSystem;
	CTCPReceiveProcessor* TCPReceiveProcessor;
	CUDPReceiveProcessor* UDPReceiveProcessor;

	// TCP
	CSocket* _tcpListenSocket;
	DWORD _tcpAcceptLen;

	// UDP
	SOCKET _udpRecvSocket;
	FSocketAddrIn _udpServeraddr;

	void WriteLog(const ELogLevel& level, const std::string& msg);

	static DWORD WINAPI WorkerThread(LPVOID arg);

	// 중복 close 처리를 막기위해 소켓마다 ON OFF 처리를 하는 map.
	map<CSocket*, bool> sockStates;

	// overlapped 타입에 대한 처리 : TCP
	void _AcceptProc(FAcceptOverlapped* overlap);
	void _RecvProc(FBuffuerableOverlapped* overlap, const int& len, bool isTCP);
	void _SendProc(FSendOverlapped* overlap);
	void _CloseProc(FCloseOverlapped* overlap);

	// 초기화 관련 함수
	bool InitWorkerThread();
	bool InitTCPListenSocket();
	bool InitUDPListenSocket();
	bool InitIOCP();
	bool InitObjectPools();

	static FSocketAddrIn* ADDRToADDRIN(FSocketAddr* addr);

	// Object Pools
	CObjectPool<FAcceptOverlapped>* _acceptOverlapObjects = nullptr;
	CObjectPool<FSendOverlapped>* _sendOverlapObjects = nullptr;
	CObjectPool<FUDPRecvOverlapped>* _udpRecvOverlapObjects = nullptr;
	CObjectPool<FTCPRecvOverlapped>* _tcpRecvOverlapObjects = nullptr;
	CObjectPool<FCloseOverlapped>* _closeOverlapObjects = nullptr;
	CObjectPool<CSocket>* _socketObjects = nullptr;
};
#endif