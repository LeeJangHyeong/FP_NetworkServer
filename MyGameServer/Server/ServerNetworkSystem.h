#pragma once
/*
	TCP/UDP 를 가동하는 싱글톤 매니저 클래스
*/

#include "MyGameServer.h"
#include "Server.h"
#include <DataType/Socket.h>
#include <string>
#include <tuple>
#include <map>
#include <memory>
#include <thread>

class CUDPProcessor;
class CPlayer;
class CSocket;

#define NONE_STEAM_PING_DELAY 3
#define PING_DELAY 10
#define PING_FINAL_DELAY 60
#define TCP_PROCESS_WAIT_MILLSEC 100

enum class ELogLevel;

class CServerNetworkSystem
{
public:
	static CServerNetworkSystem* GetInstance();
	~CServerNetworkSystem();
	bool Run();
	void Send(const std::shared_ptr<CPlayer>& player, const char* buf, const int& sendLen, bool isTCP);
	void Close(CSocket* socket);
	void SendTo(FSocketAddrIn* sockaddr, const char* buf, const int& sendLen);
	bool IsRun();

	// 플레이어 단절 로직
	void CloseConnection(std::shared_ptr<CPlayer> player);

	// from -> to로 플레이어의 정보를 넘겨준다.
	// from은 삭제되고, to에 흡수된다.
	// old에 존재하는 수정이 불가능한 파라미터를 제외하고 모두 넘어간다.
	std::shared_ptr<CPlayer> AbsorbPlayer(std::shared_ptr<CPlayer> from_new, std::shared_ptr<CPlayer> to_old);

private:
	static CServerNetworkSystem* _instance;

	CServerNetworkSystem();
	void WriteLog(const ELogLevel& level, const std::string& msg);

	// IOCP or EPOLL
	IServer* _server;

	// 핑과 관련된 서버 주체의 주기적 로직은 여기서 다룬다.
	void ServerProcessThread();

	// 서버 로직을(핑, 매칭) 돌리는 쓰레드
	std::thread* serverProcTh = nullptr;
};