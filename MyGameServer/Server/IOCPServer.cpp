#ifdef _WIN32
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"ws2_32.lib")
#pragma comment(lib,"mswsock.lib") 
#include "IOCPServer.h"
#include "Server/ServerNetworkSystem.h"
#include "TCP/TCPReceiveProcessor.h"
#include "UDP/UDPReceiveProcessor.h"
#include "NetworkModule/NetworkData.h"
#include "NetworkModule/Log.h"
#include "NetworkModule/Serializer.h"
#include "Content/PlayerManager.h"
#include "Content/RoomManager.h"
#include "NetworkModule/MyTool.h"
#include <WinSock2.h>
#include <MSWSock.h>
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <tuple>
#include <chrono>
#include <ctime> 


using namespace MyTool;
using namespace MySerializer;

CIOCPServer* CIOCPServer::_instance = nullptr;

CIOCPServer::CIOCPServer() : _bIsRun(false)
{}


CIOCPServer* CIOCPServer::GetInstance()
{
	if (!_instance) _instance = new CIOCPServer();
	return _instance;
}

CIOCPServer::~CIOCPServer()
{
	if (_socketObjects) delete _socketObjects;
	if (_acceptOverlapObjects) delete _acceptOverlapObjects;
	if (_sendOverlapObjects) delete _sendOverlapObjects;
	if (_udpRecvOverlapObjects) delete _udpRecvOverlapObjects;
	if (_tcpRecvOverlapObjects) delete _tcpRecvOverlapObjects;
}

void CIOCPServer::WriteLog(const ELogLevel& level, const std::string& msg)
{
#ifdef DEBUG_RECV_MSG
	std::cout << msg << std::endl;
#endif
	CLog::WriteLog(ELogType::IOCPServer, level, msg);
}

bool CIOCPServer::Run()
{
	RoomManager = CRoomManager::GetInstance();
	PlayerManager = CPlayerManager::GetInstance();
	ServerNetworkSystem = CServerNetworkSystem::GetInstance();
	TCPReceiveProcessor = CTCPReceiveProcessor::GetInstance();
	UDPReceiveProcessor = CUDPReceiveProcessor::GetInstance();

	if (IsRun()) return false;
	_bIsRun = true;

	// Init WSA
	WSAData wsa;
	if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0 || !InitIOCP()) return false;

	// 초기화
	if (!InitObjectPools() || !InitTCPListenSocket() || !InitWorkerThread() || !InitUDPListenSocket()) {
		_bIsRun = false;
		return false;
	}

	// 첫 TCP Accept 시작
	PostAccept();

	// 첫 UDP Recv 시작
	PostRecv(nullptr, false);

	return true;
}

void CIOCPServer::PostClose(CSocket* socket)
{
	if (!socket || !socket->GetSocket() || sockStates[socket] == false) return;

	// 같은 소켓을 또다시 단절하는 일이 없도록 체크한다.
	sockStates[socket] = false;

	FCloseOverlapped* overlap = _closeOverlapObjects->PopObject();
	overlap->socket = socket;

	// 단절을 요청한다.
	if (TransmitFile(overlap->socket->GetSocket(),
		0, 0, 0, overlap, 0, TF_DISCONNECT | TF_REUSE_SOCKET) == FALSE)
	{
		DWORD error = WSAGetLastError();
		if (error != WSA_IO_PENDING)
		{
			_socketObjects->ReturnObject(socket);
			_closeOverlapObjects->ReturnObject(overlap);
			WriteLog(ELogLevel::Warning, CLog::Format("Failed to close. Error : ", WSAGetLastError()));
			return;
		}
	}
}

void CIOCPServer::PostAccept()
{
	FAcceptOverlapped* overlap = _acceptOverlapObjects->PopObject();
	CSocket* newSocket = _socketObjects->PopObject();

	overlap->socket = newSocket;

	WriteLog(ELogLevel::Warning, "Start accept.");

	// accept
	if (AcceptEx(_tcpListenSocket->GetSocket(), newSocket->GetSocket(), (LPVOID)&(overlap->m_acceptBuf), 0,
		sizeof(overlap->m_acceptBuf.m_pLocal), sizeof(overlap->m_acceptBuf.m_pRemote), &_tcpAcceptLen, overlap) == FALSE) {
		if (WSAGetLastError() != WSA_IO_PENDING)
		{
			PostClose(newSocket);
			WriteLog(ELogLevel::Warning, CLog::Format("Failed to accept. Error : ", WSAGetLastError()));
			_bIsRun = false;
		}
	}
}

void CIOCPServer::PostRecv(FBuffuerableOverlapped* overlap, bool isTCP)
{
	DWORD recvBytes;
	DWORD flags = 0;
	if (isTCP) {
		if (!overlap) return;
		FTCPRecvOverlapped* tcpOverlap = (FTCPRecvOverlapped*)overlap;

		tcpOverlap->wsabuf.len = PACKET_BUFSIZE;

		// TCP는 WSARecv()를 사용하여 플레이어의 소켓에 Recv를 요청한다.
		int retval = WSARecv(tcpOverlap->m_player->socket->GetSocket(), &tcpOverlap->wsabuf, 1,
			&recvBytes, &flags, tcpOverlap, NULL);
		if (retval == SOCKET_ERROR) {
			if (WSAGetLastError() != WSA_IO_PENDING) {
				WriteLog(ELogLevel::Warning, CLog::Format("WSARecv Error %d", WSAGetLastError()));

				// Close and logging
				ServerNetworkSystem->CloseConnection(tcpOverlap->m_player);
				return;
			}
		}
	}
	else {
		FUDPRecvOverlapped* udpOverlap = nullptr;
		if (overlap) udpOverlap = (FUDPRecvOverlapped*)overlap;

		if (udpOverlap == nullptr) {
			udpOverlap = _udpRecvOverlapObjects->PopObject();
			udpOverlap->wsabuf.len = PACKET_BUFSIZE;
		}

		// UDP는 WSARecvFrom을 사용하여, 플레이어의 Addr에 RecvFrom을 요청한다.
		udpOverlap->udpAddr = _udpServeraddr;
		int len = sizeof(udpOverlap->udpAddr);
		if (WSARecvFrom(_udpRecvSocket, &udpOverlap->wsabuf, 1,
			&recvBytes, &flags, (sockaddr*)&udpOverlap->udpAddr, &len, udpOverlap, NULL) == SOCKET_ERROR) {
			if (WSAGetLastError() != WSA_IO_PENDING) {
				WriteLog(ELogLevel::Warning, CLog::Format("WSARecvFrom Error %d", WSAGetLastError()));
				PostRecv(false, (FBuffuerableOverlapped*)udpOverlap);
				return;
			}
		}
	}
}

void CIOCPServer::_AcceptProc(FAcceptOverlapped * overlap)
{
	// 소켓 옵션을 활성화 상태로 바꾼다.
	SOCKET listenSocket = _tcpListenSocket->GetSocket();

	if (setsockopt(overlap->socket->GetSocket(), SOL_SOCKET, SO_UPDATE_ACCEPT_CONTEXT,
		(const char *)&listenSocket, sizeof(SOCKET)) == SOCKET_ERROR)
	{
		std::string clientAcceptLog = CLog::Format("Can't set update accept context sockopt in _AcceptProc.");
		WriteLog(ELogLevel::Warning, clientAcceptLog);
		PostClose(overlap->socket);
		_acceptOverlapObjects->ReturnObject(overlap);
		return;
	}

	// Accept한 소켓의 addr을 받아온다.
	FSocketAddr *local_addr, *remote_addr;
	int l_len = 0, r_len = 0;
	GetAcceptExSockaddrs(&overlap->m_acceptBuf, 0, sizeof(overlap->m_acceptBuf.m_pLocal),
		sizeof(overlap->m_acceptBuf.m_pRemote), &local_addr, &l_len, &remote_addr, &r_len);
	FSocketAddrIn clientaddr = *(ADDRToADDRIN(remote_addr));

	std::string clientAcceptLog = CLog::Format("[Accept] IP=%s, PORT=%d",
		inet_ntoa(clientaddr.sin_addr),
		ntohs(clientaddr.sin_port));
	WriteLog(ELogLevel::Warning, clientAcceptLog);

	// 플레이어를 생성한다.
	int addrlen = sizeof(clientaddr);
	std::shared_ptr<CPlayer> newPlayer(new CPlayer());
	newPlayer->socket = overlap->socket;
	newPlayer->steamID = 0;
	newPlayer->lastPingTime = std::chrono::system_clock::now();
	newPlayer->lastPongTime = std::chrono::system_clock::now();
	newPlayer->state = LOBBY;
	newPlayer->udpAddr = nullptr;
	newPlayer->addr = clientaddr;
	PlayerManager->AddPlayer(newPlayer);
	sockStates[newPlayer->socket] = true;
	_acceptOverlapObjects->ReturnObject(overlap);

	// 새로운 방을 만들어서 플레이어에게 할당한다.
	RoomManager->CreateRoom(newPlayer);

	// 수신을 위한 overlapped를 준비한다.
	FTCPRecvOverlapped* recvOverlap = new FTCPRecvOverlapped(newPlayer);
	recvOverlap->wsabuf.len = PACKET_BUFSIZE;

	// IOCP를 연결해준다.
	CreateIoCompletionPort((HANDLE)newPlayer->socket->GetSocket(), hcp, newPlayer->socket->GetSocket(), 0);

	// Recv를 실행한다.
	DWORD recvbytes, flags = 0;
	if (WSARecv(newPlayer->socket->GetSocket(), &recvOverlap->wsabuf, 1, &recvbytes,
		&flags, recvOverlap, NULL) == SOCKET_ERROR) {
		if (WSAGetLastError() != ERROR_IO_PENDING) {
			WriteLog(ELogLevel::Error, CLog::Format("Failed to recv : %d", WSAGetLastError()));
		}
	}

	// 다시 Accept을 준비한다.
	PostAccept();
}

void CIOCPServer::_RecvProc(FBuffuerableOverlapped* overlap, const int& len, bool isTCP)
{
	if (isTCP) {
		FTCPRecvOverlapped* tcpOverlap = (FTCPRecvOverlapped*)overlap;
		// TCP 수신 데이터를 까서 로직을 처리하는 클래스에 넘긴다.
		if (!TCPReceiveProcessor->ReceiveData(tcpOverlap->m_player, tcpOverlap->buf, len)) {
			std::string errorLog = CLog::Format("[TCP ReceiveData Error] %s\n", inet_ntoa(tcpOverlap->m_player->addr.sin_addr));
			printf_s("%s", errorLog.c_str());
			WriteLog(ELogLevel::Error, errorLog);
			ServerNetworkSystem->CloseConnection(tcpOverlap->m_player);
		}
		else PostRecv(overlap, true);
	}
	else {
		FUDPRecvOverlapped* udpOverlap = (FUDPRecvOverlapped*)overlap;
		// UDP 수신 데이터를 까서 로직을 처리하는 클래스에 넘긴다.
		if (!UDPReceiveProcessor->ReceiveData(udpOverlap->udpAddr, udpOverlap->buf, len)) {
			std::string errorLog = CLog::Format("[UDP ReceiveData Error] %s\n", inet_ntoa(udpOverlap->udpAddr.sin_addr));
			printf_s("%s", errorLog.c_str());
			WriteLog(ELogLevel::Error, errorLog);
		}
		// UDP는 오류가나도 계속 진행한다.
		PostRecv(overlap, false);
	}
}

void CIOCPServer::_SendProc(FSendOverlapped* overlap)
{
	_sendOverlapObjects->ReturnObject(overlap);
}

void CIOCPServer::_CloseProc(FCloseOverlapped* overlap)
{
	// 오브젝트를 반납한다.
	_socketObjects->ReturnObject(overlap->socket);
	_closeOverlapObjects->ReturnObject(overlap);
}

FSocketAddrIn* CIOCPServer::ADDRToADDRIN(FSocketAddr* addr)
{
	return (FSocketAddrIn*)addr;
}

bool CIOCPServer::InitWorkerThread()
{
	// Check CPU
	SYSTEM_INFO si;
	GetSystemInfo(&si);

	// Create Worker Thread
	HANDLE hThread;
	for (int i = 0; i < (int)si.dwNumberOfProcessors * 2; ++i) {
		hThread = CreateThread(NULL, 0, WorkerThread, (LPVOID*)this, 0, nullptr);
		if (hThread == nullptr) {
			WriteLog(ELogLevel::Error, "Failed to Create Thread");
			return false;
		}
		CloseHandle(hThread);
	}
	return true;
}

bool CIOCPServer::InitTCPListenSocket()
{
	// Create Listen Socket
	_tcpListenSocket = _socketObjects->PopObject();

	// Bind socket to IOCP
	CreateIoCompletionPort((HANDLE)_tcpListenSocket->GetSocket(), hcp, _tcpListenSocket->GetSocket(), 0);

	// Bind
	FSocketAddrIn serveraddr;
	ZeroMemory(&serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
	serveraddr.sin_port = htons(TCP_SERVER_PORT);
	if (bind(_tcpListenSocket->GetSocket(), (FSocketAddr*)&serveraddr, sizeof(serveraddr)) == SOCKET_ERROR) {
		WriteLog(ELogLevel::Error, "Failed to bind.");
		return false;
	}

	int enable = 0;
	if (setsockopt(_tcpListenSocket->GetSocket(), SOL_SOCKET, SO_CONDITIONAL_ACCEPT, (const char*)&enable, sizeof(int)) == SOCKET_ERROR)
	{
		WriteLog(ELogLevel::Warning, CLog::Format("setsockopt so_conditional_accept Error : ", WSAGetLastError()));
		return false;
	}

	int rcvBufSize = PACKET_BUFSIZE;
	if (setsockopt(_tcpListenSocket->GetSocket(), SOL_SOCKET, SO_RCVBUF, (const char*)&rcvBufSize, sizeof(int)) == SOCKET_ERROR)
	{
		WriteLog(ELogLevel::Warning, CLog::Format("setsockopt SO_RCVBUF Error : ", WSAGetLastError()));
		return false;
	}

	// listen
	if (listen(_tcpListenSocket->GetSocket(), SOMAXCONN) == SOCKET_ERROR) {
		WriteLog(ELogLevel::Error, "Failed to listen.");
		return false;
	}
	return true;
}

bool CIOCPServer::InitUDPListenSocket()
{
	_udpRecvSocket = WSASocket(PF_INET, SOCK_DGRAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);

	ZeroMemory(&_udpServeraddr, sizeof(FSocketAddrIn));
	_udpServeraddr.sin_family = AF_INET;
	_udpServeraddr.sin_addr.s_addr = htonl(INADDR_ANY);
	_udpServeraddr.sin_port = htons(UDP_SERVER_PORT);
	if (bind(_udpRecvSocket, (FSocketAddr*)&_udpServeraddr, sizeof(_udpServeraddr)) == SOCKET_ERROR) {
		WriteLog(ELogLevel::Error, "Failed to bind.");
		return false;
	}

	int rcvBufSize = PACKET_BUFSIZE;
	if (setsockopt(_udpRecvSocket, SOL_SOCKET, SO_RCVBUF, (const char*)&rcvBufSize, sizeof(int)) == SOCKET_ERROR)
	{
		WriteLog(ELogLevel::Warning, CLog::Format("setsockopt SO_RCVBUF Error : ", WSAGetLastError()));
		return false;
	}

	CreateIoCompletionPort((HANDLE)_udpRecvSocket, hcp, _udpRecvSocket, 0);

	return true;
}

bool CIOCPServer::InitIOCP()
{
	// Create IOCP
	hcp = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, 0, 0);
	if (hcp == nullptr) {
		WriteLog(ELogLevel::Error, "Failed to Create IOCP");
		return false;
	}
	return true;
}

bool CIOCPServer::InitObjectPools()
{
	_socketObjects = new CObjectPool<CSocket>;
	_acceptOverlapObjects = new CObjectPool<FAcceptOverlapped>;
	_sendOverlapObjects = new CObjectPool<FSendOverlapped>;
	_udpRecvOverlapObjects = new CObjectPool<FUDPRecvOverlapped>;
	_tcpRecvOverlapObjects = new CObjectPool<FTCPRecvOverlapped>;
	_closeOverlapObjects = new CObjectPool<FCloseOverlapped>;
	return true;
}

DWORD WINAPI CIOCPServer::WorkerThread(LPVOID arg)
{
	int retval;
	CIOCPServer* owner = (CIOCPServer*)arg;
	HANDLE& hcp = owner->hcp;

	DWORD cbTransferred;
	SOCKET client_sock;
	FOverlapped* overlap;

	while (true) {
		// Wait until Async IO end
		retval = GetQueuedCompletionStatus(hcp, &cbTransferred, &client_sock,
			(LPOVERLAPPED *)&overlap, INFINITE);

		try {
			switch (overlap->m_type)
			{
			case EOverlappedType::ACCEPT:
			{
				owner->_AcceptProc((FAcceptOverlapped*)overlap);
				break;
			}
			case EOverlappedType::TCP_RECV:
			{
				owner->_RecvProc((FBuffuerableOverlapped*)overlap, cbTransferred, true);
				break;
			}
			case EOverlappedType::UDP_RECV:
			{
				owner->_RecvProc((FBuffuerableOverlapped*)overlap, cbTransferred, false);
				break;
			}
			case EOverlappedType::SEND:
			{
				owner->_SendProc((FSendOverlapped*)overlap);
				break;
			}
			case EOverlappedType::CLOSE:
			{
				owner->_CloseProc((FCloseOverlapped*)overlap);
				break;
			}
			}
		}
		catch (...) {
			owner->WriteLog(ELogLevel::Error, CLog::Format("[ReceiveData Exception] %s", inet_ntoa(overlap->m_player->addr.sin_addr)));
			owner->ServerNetworkSystem->CloseConnection(overlap->m_player);
			delete overlap;
		}
	}
	return 0;
}

void CIOCPServer::Shutdown()
{
	_bIsRun = false;
	WSACleanup();
}

void CIOCPServer::PostSend(const std::shared_ptr<CPlayer>& player, const char* buf, const int& sendLen, bool isTCP)
{
	FSendOverlapped* overlap = _sendOverlapObjects->PopObject();

	// 앞에 총 길이를 더해서 보낸다.
	int realSendLen = sendLen + sizeof(int);

	// 버퍼를 만든다.
	MySerializer::IntSerialize(overlap->wsabuf.buf, sendLen);
	memcpy(overlap->buf + sizeof(int), buf, sendLen);
	overlap->wsabuf.len = realSendLen;

	// TCP 플래그거나, Player의 UDP 주소가 없으면 WSASend를 수행한다.
	if (isTCP || player->udpAddr == nullptr) {
		if (WSASend(player->socket->GetSocket(), &overlap->wsabuf, 1, &overlap->wsabuf.len, 0, overlap, NULL) == SOCKET_ERROR)		// 데이터 전송
		{
			if (WSAGetLastError() != WSA_IO_PENDING)
			{
				ServerNetworkSystem->CloseConnection(player);
				WriteLog(ELogLevel::Warning, CLog::Format("Failed to WSASend. Error : ", WSAGetLastError()));
				return;
			}
		}
	}
	// UDP는 WSASendTo를 사용하여 전송한다.
	else {
		if (WSASendTo(_udpRecvSocket,
			&overlap->wsabuf, 1,
			&overlap->wsabuf.len, 0,
			(FSocketAddr*)player->udpAddr.get(), sizeof(FSocketAddrIn),
			overlap, NULL) == SOCKET_ERROR)		// 데이터 전송
		{
			if (WSAGetLastError() != WSA_IO_PENDING)
			{
				ServerNetworkSystem->CloseConnection(player);
				WriteLog(ELogLevel::Warning, CLog::Format("Failed to WSASendTo Error : ", WSAGetLastError()));
				return;
			}
		}
	}
}

void CIOCPServer::PostUDPSend(FSocketAddrIn* sockaddr, const char* buf, const int& sendLen)
{
	FSendOverlapped* overlap = _sendOverlapObjects->PopObject();

	// 앞에 총 길이를 더해서 보낸다.
	int realSendLen = sendLen + sizeof(int);

	// 버퍼를 만든다.
	overlap->wsabuf.buf = overlap->buf;
	MySerializer::IntSerialize(overlap->buf, sendLen);
	memcpy(overlap->buf + sizeof(int), buf, sendLen);
	overlap->wsabuf.len = realSendLen;

	if (WSASendTo(_udpRecvSocket,
		&overlap->wsabuf, 1,
		&overlap->wsabuf.len, 0,
		(FSocketAddr*)sockaddr, sizeof(FSocketAddrIn),
		overlap, NULL) == SOCKET_ERROR)		// 데이터 전송
	{
		if (WSAGetLastError() != WSA_IO_PENDING)
		{
			WriteLog(ELogLevel::Warning, CLog::Format("Failed to WSASendTo Error : ", WSAGetLastError()));
			return;
		}
	}
}

bool CIOCPServer::IsRun()
{
	return _bIsRun;
}

void CIOCPServer::Send(const std::shared_ptr<CPlayer>& player, const char* buf, const int& sendLen, bool isTCP)
{
	PostSend(player, buf, sendLen, isTCP);
}

void CIOCPServer::Close(CSocket* socket)
{
	PostClose(socket);
}

void CIOCPServer::SendTo(FSocketAddrIn* sockaddr, const char* buf, const int& sendLen)
{
	PostUDPSend(sockaddr, buf, sendLen);
}
#endif