﻿#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include "IOCPServer.h"
#include "EpollServer.h"
#include "Server/ServerNetworkSystem.h"
#include "UDP/UDPReceiveProcessor.h"
#include "NetworkModule/Log.h"
#include "DataType/Player.h"
#include "DataType/Socket.h"
#include "Content/RoomManager.h"
#include "NetworkModule/Serializer.h"
#include <iostream>
#include <thread>

using namespace MySerializer;

CServerNetworkSystem* CServerNetworkSystem::_instance = nullptr;
CServerNetworkSystem::CServerNetworkSystem()
{
#ifdef _WIN32 // WINDOW
	_server = CIOCPServer::GetInstance();
#elif __linux__ // LINUX
	_server = CEpollServer::GetInstance();
#endif
}

CServerNetworkSystem * CServerNetworkSystem::GetInstance()
{
	if (_instance == nullptr) _instance = new CServerNetworkSystem();
	return _instance;
}

CServerNetworkSystem::~CServerNetworkSystem()
{
	if (serverProcTh != nullptr) {
		delete serverProcTh;
		serverProcTh = nullptr;
	}
}

bool CServerNetworkSystem::Run()
{
	std::chrono::seconds sleepDuration(2);
	WriteLog(ELogLevel::Warning, "Run statred....");

	// Run Server
	if (!_server->Run()) return false;

	// 서버단 로직을 담당하는 쓰레드 실행
	serverProcTh = new std::thread(&CServerNetworkSystem::ServerProcessThread, this);

	return true;
}

void CServerNetworkSystem::Send(const std::shared_ptr<CPlayer>& player, const char* buf, const int& sendLen, bool isTCP)
{
	_server->Send(player, buf, sendLen, isTCP);
}

void CServerNetworkSystem::Close(CSocket* socket)
{
	_server->Close(socket);
}

void CServerNetworkSystem::SendTo(FSocketAddrIn* sockaddr, const char* buf, const int& sendLen)
{
	_server->SendTo(sockaddr, buf, sendLen);
}

bool CServerNetworkSystem::IsRun()
{
	return _server->IsRun();
}

void CServerNetworkSystem::CloseConnection(std::shared_ptr<CPlayer> player)
{
	if (!player) return;

	// 방에 단절을 통보한다.
	CRoomManager::GetInstance()->DisconnectRoom(player);

	// 소켓을 단절시킨다.
	if (player->socket) Close(player->socket);

	std::string closeLog = CLog::Format("[Close] IP = %s, Port = %d",
		inet_ntoa(player->addr.sin_addr),
		ntohs(player->addr.sin_port));
	CLog::WriteLog(ELogType::ServerNetworkSystem, ELogLevel::Warning, closeLog);
}

std::shared_ptr<CPlayer> CServerNetworkSystem::AbsorbPlayer(std::shared_ptr<CPlayer> from_new, std::shared_ptr<CPlayer> to_old)
{
	if (from_new.get() == to_old.get()) return from_new;

	// 흡수전 방을 나간다.
	CRoomManager::GetInstance()->OutRoom(from_new);

	// 흡수할 플레이어의 소켓은 파괴시킨다.
	if (to_old->socket) {
		Close(to_old->socket);
		to_old->socket = nullptr;
	}

	CPlayerManager::GetInstance()->AbsorbPlayer(from_new, to_old);

	std::string closeLog = CLog::Format("[Close] IP = %s, Port = %d",
		inet_ntoa(to_old->addr.sin_addr),
		ntohs(to_old->addr.sin_port));
	CLog::WriteLog(ELogType::ServerNetworkSystem, ELogLevel::Warning, closeLog);

	return to_old;
}

void CServerNetworkSystem::WriteLog(const ELogLevel& level, const std::string& msg)
{
	CLog::WriteLog(ELogType::ServerNetworkSystem, level, msg);
}

void CServerNetworkSystem::ServerProcessThread()
{
	std::chrono::milliseconds sleepDuration(TCP_PROCESS_WAIT_MILLSEC);

	auto PlayerManager = CPlayerManager::GetInstance();
	auto RoomManager = CRoomManager::GetInstance();
	
	while (IsRun()) {
		int num = -1;
		while (true) {
			++num;
			try {
				std::chrono::system_clock::time_point currentTime = std::chrono::system_clock::now();
				std::shared_ptr<CPlayer> player = PlayerManager->GetPlayerByNum(num);
				if (player == nullptr) break;
				std::chrono::seconds pingDelay = std::chrono::duration_cast<std::chrono::seconds>(currentTime - player->lastPingTime);
				std::chrono::seconds pongDelay = std::chrono::duration_cast<std::chrono::seconds>(currentTime - player->lastPongTime);
				// 일정 시간 이상 응답이 없다면 연결을 끊는다.
				if (pongDelay.count() >= PING_FINAL_DELAY) {
					WriteLog(ELogLevel::Error, CLog::Format("[%s:%d] : Close Connection By Ping FAILED.\n", inet_ntoa(player->addr.sin_addr),
						ntohs(player->addr.sin_port)));
					CloseConnection(player);
				}
				// 스팀아이디가 없다면 핑대신 스팀 요청을 보낸다.
				else if (player->steamID == 0 && pingDelay.count() >= NONE_STEAM_PING_DELAY) {
					WriteLog(ELogLevel::Warning, CLog::Format("[%s:%d] : STEAM REQUEST! \n", inet_ntoa(player->addr.sin_addr),
						ntohs(player->addr.sin_port)));

					char sendBuf[sizeof(EMessageType)];
					SerializeEnum(S_Common_RequestId, sendBuf);
					Send(player, sendBuf, sizeof(EMessageType), true);
					player->lastPingTime = currentTime;
				}
				// 스팀아이디가 있다면 핑만 보낸다.
				else if (player->steamID != 0 && pingDelay.count() >= PING_DELAY) {
					WriteLog(ELogLevel::Warning, CLog::Format("[%s:%d] : PING! \n", inet_ntoa(player->addr.sin_addr),
						ntohs(player->addr.sin_port)));

					char sendBuf[sizeof(EMessageType)];
					SerializeEnum(COMMON_PING, sendBuf);
					Send(player, sendBuf, sizeof(EMessageType), true);
					player->lastPingTime = currentTime;
				}
			}
			catch (std::exception& e) {
				// throw을 삼키고, 로그를 출력후 계속 진행.
				std::string newLog = CLog::Format("[ServerProcessThread:Exception] : %s", e.what());
				printf("%s\n", newLog.c_str());
				CLog::WriteLog(ELogType::ServerNetworkSystem, ELogLevel::Error, newLog);
			}
		}

		RoomManager->Update();

		// 완료되면 n초 쉰다.
		//std::this_thread::sleep_for(sleepDuration);
	}
}
