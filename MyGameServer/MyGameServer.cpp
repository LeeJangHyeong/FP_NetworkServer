#include "Server/ServerNetworkSystem.h"
#include "UDP/UDPReceiveProcessor.h"
#include "Content/RoomManager.h"
#include "Content/PlayerManager.h"
#include "NetworkModule/Log.h"
#include "NetworkModule/Serializer.h"
#include "NetworkModule/NetworkData.h"
#include "NetworkModule/MyTool.h"
#include <thread>
#include <iostream>

void ClearScreen()
{
#ifdef _WIN32
	system("cls");
#elif __linux__
	system("clear");
#endif
}

int main()
{
	CLog::WriteLog(ELogType::NetworkManager, ELogLevel::Warning, CLog::Format("Game Server Start"));
	CServerNetworkSystem* ServerSystem = CServerNetworkSystem::GetInstance();
	std::chrono::seconds sleepDuration(1);

	try
	{
		if (!ServerSystem->Run()) {
			std::cout << "FAIL!!";
			throw;
		}
		std::cout << "Server is now on playable.\n";

		auto RoomManager = CRoomManager::GetInstance();
		auto PlayerManager = CPlayerManager::GetInstance();

		while (true) {

			// Print Room Data
#ifdef DEBUG_RECV_MSG
			/*std::cout << "R" << RoomManager->GetPlayerCount() << " "
				<< "MR" << RoomManager->GetMatchRoomCount() << " "
				<< "GR" << RoomManager->GetGameRoomCount() << " "
				<< "P" << PlayerManager->GetPlayerCount() << "\n";*/
			std::this_thread::sleep_for(sleepDuration);
#else
			// Check Server State
			if (!ServerSystem->IsRun()) {
				throw;
			}

			std::cout << "R" << RoomManager->GetPlayerCount() << " "
				<< "MR" << RoomManager->GetMatchRoomCount() << " "
				<< "GR" << RoomManager->GetGameRoomCount() << " "
				<< "P" << PlayerManager->GetPlayerCount() << "\n";
			std::this_thread::sleep_for(sleepDuration);
			ClearScreen();
#endif
		}
	}
	catch (const std::exception& e)
	{
		CLog::WriteLog(ELogType::NetworkManager, ELogLevel::Critical, CLog::Format("Game Server End by ERROR: %s", e.what()));
		CLog::Join();
	}
	CLog::Join();

	CLog::WriteLog(ELogType::NetworkManager, ELogLevel::Warning, CLog::Format("Game Server End Successfully."));
	
	return 0;
}