#define  _WINSOCK_DEPRECATED_NO_WARNINGS
#include "TCPReceiveProcessor.h"
#include "Server/ServerNetworkSystem.h"
#include "Content/PlayerManager.h"
#include "Content/RoomManager.h"
#include "DataType/Room.h"
#include "NetworkModule/NetworkData.h"
#include "NetworkModule/Serializer.h"
#include "NetworkModule/Log.h"
#include "NetworkModule/MyTool.h"
#include <stdio.h>
#include <chrono>
#include <ctime> 
#include <memory>

using namespace MyTool;
using namespace std;
using namespace MySerializer;

typedef lock_guard<mutex> Lock;

CTCPReceiveProcessor* CTCPReceiveProcessor::_instance = nullptr;

CTCPReceiveProcessor* CTCPReceiveProcessor::GetInstance()
{
	if (_instance == nullptr) _instance = new CTCPReceiveProcessor();
	return _instance;
}

bool CTCPReceiveProcessor::ReceiveData(std::shared_ptr<CPlayer>& player, char* recvBuf, int receiveLen)
{
	if (receiveLen < 0 || receiveLen > PACKET_BUFSIZE) return false;
	if (!player) {
		std::cout << "[CTCPReceiveProcessor::ReceiveData] Critical Error!!!!\n";
		return false;
	}

	// 남은 버퍼가 있는지 확인하고 있다면 꺼내서 현재 버퍼에 합쳐준다.
	if (player->delayedTCPDataLen > 0) {
		player->PopDelayData(recvBuf, receiveLen, true);
	}

	int cursor = 0, bufLen = 0;
	EMessageType type;
	while (cursor < receiveLen) {
		// ENUM을 제외한 길이를 계산한다.
		bufLen = IntDeserialize(recvBuf, &cursor) - (int)sizeof(EMessageType);
		if (bufLen < 0) {
			WriteLog(ELogLevel::Error, CLog::Format("TCP Receive buflen < 0"));
			return false;
		}
		if (bufLen > PACKET_BUFSIZE) {
			WriteLog(ELogLevel::Error, CLog::Format("TCP Receive buflen > BUFSIZE, bufSize = %d", bufLen));
			return false;
		}

		// 수신량을 넘어서 데이터가 넘어온다면 남은 데이터를 저장하고 리턴한다.
		if (cursor + bufLen + (int)sizeof(EMessageType) > receiveLen) {
			cursor -= (int)sizeof(INT32);
			player->PushDelayData(recvBuf, cursor, receiveLen, true);
			return true;
		}

		type = GetEnum(recvBuf, &cursor);

		std::string recvLog = CLog::Format("[%s:%d] : Recv type : %d\n",
			inet_ntoa(player->addr.sin_addr),
			ntohs(player->addr.sin_port), type);
		WriteLog(ELogLevel::Warning, recvLog);

		switch (type)
		{
		case COMMON_ECHO:
		{
			Common_Echo(player, recvBuf, cursor, bufLen);
			break;
		}
		case COMMON_PING:
		{
			Common_Ping(player, recvBuf, cursor, bufLen);
			break;
		}
		case C_Common_AnswerId:
		{
			Common_AnswerId(player, recvBuf, cursor, bufLen);
			break;
		}
		case C_Lobby_InviteFriend_Request:
		{
			Lobby_InviteFriendRequest(player, recvBuf, cursor, bufLen);
			break;
		}

		case C_Lobby_InviteFriend_Answer:
		{
			Lobby_InviteFriendAnswer(player, recvBuf, cursor, bufLen);
			break;
		}
		case C_Lobby_Set_PartyKing:
		{
			Lobby_SetPartyKing(player, recvBuf, cursor, bufLen);
			break;
		}
		case C_Lobby_FriendKick_Request:
		{
			Lobby_FriendKickRequest(player, recvBuf, cursor, bufLen);
			break;
		}
		case C_Lobby_MatchRequest:
		{
			Lobby_MatchRequest(player, recvBuf, cursor, bufLen);
			break;
		}
		case C_Debug_RoomStart:
		{
			Debug_RoomStart(player, recvBuf, cursor, bufLen);
			break;
		}
		case C_Debug_GameStart:
		{
			Debug_GameStart(player, recvBuf, cursor, bufLen);
			break;
		}
		case C_INGAME_SPAWN:
		{
			InGame_Spawn(player, recvBuf, cursor, bufLen);
			break;
		}
		case C_INGAME_RPC:
		{
			InGame_RPC(player, recvBuf, cursor, bufLen);
			break;
		}
		case C_INGAME_SyncVar:
		{
			InGame_SyncVar(player, recvBuf, cursor, bufLen);
			break;
		}
		case C_RECONNECT_SERVER:
		{
			InGame_ReconnectServer(player, recvBuf, cursor, bufLen);
			break;
		}
		case C_END_GAME:
		{
			InGame_EndGame(player, recvBuf, cursor, bufLen);
			break;
		}
		default:
			std::string logString = CLog::Format("Unknown syncvar type!!!! : %d", (int)type);
			CLog::WriteLog(ELogType::TCPReceiveProcessor, ELogLevel::Error, logString);
			std::cout << logString << std::endl;
			cursor += bufLen;
			break;
		}
	}
	return true;
}

void CTCPReceiveProcessor::WriteLog(const ELogLevel& level, const std::string& msg)
{
	CLog::WriteLog(ELogType::TCPReceiveProcessor, level, msg);
#ifdef DEBUG_RECV_MSG
	printf("%s\n", msg.c_str());
#endif
}

CTCPReceiveProcessor::CTCPReceiveProcessor()
{
	ServerNetworkSystem = CServerNetworkSystem::GetInstance();
	PlayerManager = CPlayerManager::GetInstance();
	RoomManager = CRoomManager::GetInstance();
}


CTCPReceiveProcessor::~CTCPReceiveProcessor()
{
}

void CTCPReceiveProcessor::Common_Echo(std::shared_ptr<CPlayer>& player, char* recvBuf, int& cursor, int& bufLen)
{
	FSerializableString res = StringDeserialize(recvBuf, &cursor);
	printf("[%s:%d] : ECHO, %s\n", inet_ntoa(player->addr.sin_addr),
		ntohs(player->addr.sin_port),
		res.buf);
	cursor += res.len;
}

void CTCPReceiveProcessor::Common_Ping(std::shared_ptr<CPlayer>& player, char* recvBuf, int& cursor, int& bufLen)
{
	// 시간 갱신
	player->lastPongTime =
		std::chrono::system_clock::now();

	WriteLog(ELogLevel::Warning, CLog::Format("[%s:%d] : COMMON_PING\n", inet_ntoa(player->addr.sin_addr),
		ntohs(player->addr.sin_port)));
}

void CTCPReceiveProcessor::Common_AnswerId(std::shared_ptr<CPlayer>& player, char* recvBuf, int& cursor, int& bufLen)
{
	UINT64 steamID = UInt64Deserialize(recvBuf, &cursor);
	bool isReconnect = false;

	if (steamID == 0) return;
	// 재접속 처리 ///// 이미 존재하면서 소켓이 다를 경우(단말이 다를 경우) 흡수 한다.
	{
		shared_ptr<CPlayer> beforeUser = PlayerManager->GetPlayerById(steamID);
		if (beforeUser) { // 이미 존재하는 ID
			if (beforeUser->socket != player->socket) { // 다른 소켓
				shared_ptr<CPlayer> newPlayer = ServerNetworkSystem->AbsorbPlayer(player, beforeUser);
			}
			else {
				isReconnect = true;
			}
		}
	}

	shared_ptr<CPlayer> targetUser = PlayerManager->GetPlayerBySocket(player->socket);
	if (!targetUser) return;

	// 스팀ID 갱신
	targetUser->steamID = steamID;

	// 시간 갱신
	targetUser->lastPongTime = std::chrono::system_clock::now();

	std::shared_ptr<CRoom> currentGameRoom = RoomManager->GetGameRoom(player);
	// 게임 중인 룸에 재접속시 재접속 통보.
	if (currentGameRoom && isReconnect) {
		if (currentGameRoom) currentGameRoom->SendReconnectToAllMember(player);
	}
	// 아니라면 새로운 방 정보 전송
	else {
		std::shared_ptr<CRoom> room = RoomManager->GetRoom(targetUser);
		if (room) room->SendRoomInfoToAllMember();
	}

	WriteLog(ELogLevel::Warning, CLog::Format("[%s:%d] : C_Common_AnswerId %llu\n", inet_ntoa(player->addr.sin_addr),
		ntohs(player->addr.sin_port), steamID));
}
void CTCPReceiveProcessor::Lobby_InviteFriendRequest(std::shared_ptr<CPlayer>& player, char* recvBuf, int& cursor, int& bufLen)
{
	UINT64 steamID = UInt64Deserialize(recvBuf, &cursor);
	if (steamID == 0) return;

	// 플레이어를 찾아, 플레이어에게 수락 의사를 묻는다.
	shared_ptr<CPlayer> targetUser = PlayerManager->GetPlayerById(steamID);
	if (targetUser) {
		shared_ptr<CPlayer> sendPlayer = PlayerManager->
			GetPlayerBySocket(player->socket);
		if (!sendPlayer) return;
		UINT64 senderId = sendPlayer->steamID;
		// 초대자의 이름을 담아 보낸다.
		char senderIdBuf[sizeof(UINT64)], finalBuf[sizeof(UINT64) + sizeof(EMessageType)];
		int uintLen = UInt64Serialize(senderIdBuf, senderId);
		int allLen = SerializeWithEnum(S_Lobby_InviteFriend_Request, senderIdBuf, uintLen, finalBuf);
		ServerNetworkSystem->Send(targetUser, finalBuf, allLen, true);
		WriteLog(ELogLevel::Warning, CLog::Format("C_Lobby_InviteFriend_Request : Invite Send Success.\n"));
	}
	// 존재 하지 않는다면, 에러를 날림.
	else {
		char allBuf[sizeof(EMessageType) + 40], buf[40], msg[] = "친구를 찾는데 실패하였습니다.";
		int stringLen = (int)strlen(msg);
		StringSerialize(buf, msg, stringLen);
		int len = SerializeWithEnum(S_Lobby_InviteFriend_Failed, buf, stringLen, allBuf);
		ServerNetworkSystem->Send(player, allBuf, len, true);
		WriteLog(ELogLevel::Warning, CLog::Format("C_Lobby_InviteFriend_Request : Failed to find member.\n"));
	}
	WriteLog(ELogLevel::Warning, CLog::Format("[%s:%d] : C_Lobby_InviteFriend_Request %llu\n", inet_ntoa(player->addr.sin_addr),
		ntohs(player->addr.sin_port), steamID));
}

void CTCPReceiveProcessor::Lobby_InviteFriendAnswer(std::shared_ptr<CPlayer>& player, char* recvBuf, int& cursor, int& bufLen)
{
	bool isYes = BoolDeserialize(recvBuf, &cursor);
	UINT64 targetID = UInt64Deserialize(recvBuf, &cursor);
	shared_ptr<CPlayer> innerPlayer = PlayerManager->GetPlayerById(targetID);
	if (!innerPlayer) return;

	WriteLog(ELogLevel::Warning, CLog::Format("[%s:%d] : C_Lobby_InviteFriend_Answer by %llu\n", inet_ntoa(player->addr.sin_addr),
		ntohs(player->addr.sin_port), targetID));

	// 승락하지 않았다면 그냥 넘어간다.
	if (!isYes) return;
	// 승락했다면, 룸 이동을 시도한다.
	int outSlot;

	// 룸이동 수행.
	RoomManager->MoveRoom(innerPlayer, player, outSlot);
}

void CTCPReceiveProcessor::Lobby_SetPartyKing(std::shared_ptr<CPlayer>& player, char* recvBuf, int& cursor, int& bufLen)
{
	// 파티장으로 임명할 슬롯을 파싱한다.
	int targetSlot = IntDeserialize(recvBuf, &cursor);

	WriteLog(ELogLevel::Warning, CLog::Format("[%s:%d] : C_Lobby_Set_PartyKing by %d\n", inet_ntoa(player->addr.sin_addr),
		ntohs(player->addr.sin_port), targetSlot));

	// 방을 찾는다.
	std::shared_ptr<CRoom> innerRoom = RoomManager->GetRoom(player);
	if (innerRoom == nullptr ||							// 방이 없거나
		innerRoom->GetPlayer(0) != player ||	// 요청자가 방장이 아니거나
		targetSlot >= MAX_PLAYER ||						// 슬롯이 잘못 되었거나
		targetSlot == 0 ||
		innerRoom->GetPlayer(targetSlot) == nullptr		// 슬롯에 플레이어가 없다면 무시한다.
		) {
		return;
	}

	// 슬롯 교체
	innerRoom->ChangePartyKing(targetSlot);

	// 파티장 교체를 통보한다.
	innerRoom->SendRoomInfoToAllMember();
}

void CTCPReceiveProcessor::Lobby_FriendKickRequest(std::shared_ptr<CPlayer>& player, char* recvBuf, int& cursor, int& bufLen)
{
	// 강퇴할 슬롯을 파싱한다.
	int targetSlot = IntDeserialize(recvBuf, &cursor);

	WriteLog(ELogLevel::Warning, CLog::Format("[%s:%d] : C_Lobby_FriendKick_Request by %d\n", inet_ntoa(player->addr.sin_addr),
		ntohs(player->addr.sin_port), targetSlot));

	// 방을 찾는다.
	std::shared_ptr<CRoom> innerRoom = RoomManager->GetRoom(player);
	if (innerRoom == nullptr ||										// 방이 없거나
		targetSlot < 0 ||
		targetSlot >= MAX_PLAYER ||									// 슬롯이 잘못 되었거나
		innerRoom->GetPlayer(targetSlot) == nullptr ||				// 슬롯에 플레이어가 없거나
		!(innerRoom->GetPlayer(0) == player ||						// 요청자가 방장이면서
			innerRoom->GetPlayer(targetSlot) == player)					// 자기 자신에 대한 처리가 아니라면 무시
		) {

		WriteLog(ELogLevel::Warning, CLog::Format("Failed to kick.\n"));
		return;
	}

	// 플레이어를 기존 방에서 추방시킨다음, 새로운 방에 넣어준다.
	shared_ptr<CPlayer> kickTargetPlayer = innerRoom->GetPlayer(targetSlot);
	RoomManager->KickPlayer(kickTargetPlayer);
}

void CTCPReceiveProcessor::Lobby_MatchRequest(std::shared_ptr<CPlayer>& player, char* recvBuf, int& cursor, int& bufLen)
{
	bool isOn = BoolDeserialize(recvBuf, &cursor);
	if (player->steamID == 0) return;

	// 룸매니저에 매칭 상태 변경을 요청한다.
	RoomManager->ChangeRoomReady(player, isOn);

	WriteLog(ELogLevel::Warning, CLog::Format("[%s:%d] : C_Lobby_MatchRequest %d id : %llu\n", inet_ntoa(player->addr.sin_addr),
		ntohs(player->addr.sin_port), isOn, player->steamID));
}

void CTCPReceiveProcessor::Debug_RoomStart(std::shared_ptr<CPlayer>& player, char* recvBuf, int& cursor, int& bufLen)
{
	std::shared_ptr<CRoom> currentRoom = RoomManager->GetRoom(player);
	if (!currentRoom) return;

	// 혼자 있을때만 방이동을 한다.
	if (currentRoom->GetPlayerCount() != 1) return;
	RoomManager->ForceJoinRoom(player);

	WriteLog(ELogLevel::Warning, CLog::Format("[%s:%d] : C_Debug_RoomStart %llu\n", inet_ntoa(player->addr.sin_addr),
		ntohs(player->addr.sin_port), player->steamID));
}

void CTCPReceiveProcessor::Debug_GameStart(std::shared_ptr<CPlayer>& player, char* recvBuf, int& cursor, int& bufLen)
{
	std::shared_ptr<CRoom> currentRoom = RoomManager->GetRoom(player);

	if (!currentRoom || currentRoom->GetPlayerCount() != 1 || currentRoom->GetState() == GAME) return;

	// 게임 룸이 존재한다면 강제로 참여한다.
	bool onSuccess = RoomManager->ForceJoinGameRoom(player);

	// 게임중인 룸이 없다면, 현재룸을 게임룸으로 강제 이동한다.
	if (!onSuccess) RoomManager->ForceChangeGameState(currentRoom);

	WriteLog(ELogLevel::Warning, CLog::Format("[%s:%d] : C_Debug_GameStart %llu newRoom? : %d\n", inet_ntoa(player->addr.sin_addr),
		ntohs(player->addr.sin_port), player->steamID, !onSuccess));
}

void CTCPReceiveProcessor::InGame_Spawn(std::shared_ptr<CPlayer>& player, char* recvBuf, int& cursor, int& bufLen)
{
	std::shared_ptr<CRoom> targetRoom = RoomManager->GetGameRoom(player);
	if (!targetRoom || targetRoom->GetState() != GAME) {
		cursor += bufLen;
		return;
	}
	// 방장이라면 그대로 전달해준다.
	if (targetRoom->GetPlayer(0) == player) {
		shared_ptr<char> pNewBuf(new char[bufLen + sizeof(EMessageType)]);
		SerializeEnum(S_INGAME_SPAWN, pNewBuf.get());
		memcpy(pNewBuf.get() + sizeof(EMessageType), recvBuf + cursor, bufLen);
		targetRoom->SendToOtherMember(player->steamID, pNewBuf.get(), bufLen + (int)sizeof(EMessageType));
	}
	cursor += bufLen;
	WriteLog(ELogLevel::Warning, CLog::Format("[%s:%d] : C_INGAME_SPAWN\n", inet_ntoa(player->addr.sin_addr),
		ntohs(player->addr.sin_port)));
}

void CTCPReceiveProcessor::InGame_EndGame(std::shared_ptr<CPlayer>& player, char* recvBuf, int& cursor, int& bufLen)
{
	// 게임 중이라면 로비로 스탯을 이동시킨뒤 종료를 통보한다.
	std::shared_ptr<CRoom> targetRoom = RoomManager->GetGameRoom(player);
	if (targetRoom && targetRoom->GetState() == GAME) {
		char buf[sizeof(EMessageType)];
		int len = SerializeEnum(EMessageType::S_END_GAME, buf);

		// 커넥션이 로스트된 플레이어는 제거한다.
		for (int i = 0; i < MAX_PLAYER; ++i) {
			auto player = targetRoom->GetPlayer(i);
			if (player && player->state == EGameState::ConnectionLost) {
				PlayerManager->RemovePlayer(player);
				targetRoom->DeletePlayer(i);
			}
		}

		RoomManager->ChangeRoomState(targetRoom, EGameState::LOBBY);
		targetRoom->SendToAllMember(buf, len);
	}

	WriteLog(ELogLevel::Warning, CLog::Format("[%s:%d] : C_END_GAME\n", inet_ntoa(player->addr.sin_addr),
		ntohs(player->addr.sin_port)));
}

void CTCPReceiveProcessor::InGame_RPC(std::shared_ptr<CPlayer>& player, char* recvBuf, int& cursor, int& bufLen)
{
	char type = CharDeserialize(recvBuf, &cursor);
	bufLen -= 1; // 캐릭터 바이트 사이즈 만큼 뺀다.
	std::shared_ptr<CRoom> targetRoom = RoomManager->GetGameRoom(player);
	if (!targetRoom || targetRoom->GetState() != GAME) {
		cursor += bufLen;
		return;
	}

	switch (type)
	{
		case 0:
		{
			// 멀티캐스트
			shared_ptr<char> pNewBuf(new char[bufLen + sizeof(EMessageType)]);

			SerializeEnum(S_INGAME_RPC, pNewBuf.get());
			memcpy(pNewBuf.get() + ((int)sizeof(EMessageType)),
				recvBuf + cursor, bufLen);
			targetRoom->SendToOtherMember(player->steamID, pNewBuf.get(), bufLen + (int)sizeof(EMessageType));
			break;
		}
		case 1:
		{
			// 마스터에서 처리.
			// 송신자가 마스터라면 무시한다.
			auto masterPlayer = targetRoom->GetPlayer(0);
			if (masterPlayer == player) break;

			// 마스터에게 전달
			shared_ptr<char> pNewBuf(new char[bufLen + sizeof(EMessageType)]);

			SerializeEnum(S_INGAME_RPC, pNewBuf.get());
			memcpy(pNewBuf.get() + (int)sizeof(EMessageType), recvBuf + cursor, bufLen);
			ServerNetworkSystem->Send(masterPlayer, pNewBuf.get(), bufLen + (int)sizeof(EMessageType), true);

			break;
		}
		default:
			WriteLog(ELogLevel::Error, CLog::Format("[%s:%d] : Unknown RPC type.\n", inet_ntoa(player->addr.sin_addr),
				ntohs(player->addr.sin_port)));
			break;
	}
	cursor += bufLen;

	WriteLog(ELogLevel::Error, CLog::Format("[%s:%d] : C_INGAME_RPC.\n", inet_ntoa(player->addr.sin_addr),
		ntohs(player->addr.sin_port)));
}

void CTCPReceiveProcessor::InGame_SyncVar(std::shared_ptr<CPlayer>& player, char* recvBuf, int& cursor, int& bufLen)
{
	std::shared_ptr<CRoom> targetRoom = RoomManager->GetGameRoom(player);
	if (!targetRoom || targetRoom->GetState() != GAME) {
		cursor += bufLen;
		return;
	}

	// 본인을 제외한 나머지 파티원에게 전달.
	shared_ptr<char> pNewBuf(new char[bufLen + sizeof(EMessageType)]);

	SerializeEnum(S_INGAME_SyncVar, pNewBuf.get());
	memcpy(pNewBuf.get() + (int)sizeof(EMessageType), recvBuf + cursor, bufLen);
	targetRoom->SendToOtherMember(player->steamID, (const char*)pNewBuf.get(), bufLen + (int)sizeof(EMessageType));

	cursor += bufLen;
}

void CTCPReceiveProcessor::InGame_ReconnectServer(std::shared_ptr<CPlayer>& player, char* recvBuf, int& cursor, int& bufLen)
{
	bool bIsOnGame = BoolDeserialize(recvBuf, &cursor);
	UINT64 steamID = UInt64Deserialize(recvBuf, &cursor);
	UINT64 steamIDs[MAX_PLAYER];
	for (int i = 0; i < MAX_PLAYER; ++i) {
		steamIDs[i] = UInt64Deserialize(recvBuf, &cursor);
	}

	bool isReconnect = false;

	if (steamID == 0) return;
	// 재접속 처리 ///// 이미 존재하면서 소켓이 다를 경우(단말이 다를 경우) 흡수 한다.
	{
		shared_ptr<CPlayer> beforeUser = PlayerManager->GetPlayerById(steamID);
		if (beforeUser) { // 이미 존재하는 ID
			if (beforeUser->socket != player->socket) { // 다른 소켓
				shared_ptr<CPlayer> newPlayer = ServerNetworkSystem->AbsorbPlayer(player, beforeUser);
			}
			else {
				isReconnect = true;
			}
		}
	}

	shared_ptr<CPlayer> targetUser = PlayerManager->GetPlayerBySocket(player->socket);
	if (!targetUser) return;

	// 스팀ID 갱신
	targetUser->steamID = steamID;

	// 시간 갱신
	targetUser->lastPongTime = std::chrono::system_clock::now();

	std::shared_ptr<CRoom> currentGameRoom = RoomManager->GetGameRoom(player);

	// 현재 룸에서 나간다.
	RoomManager->OutRoom(player);

	shared_ptr<CRoom> room;

	// 기존 멤버들중에 룸이 있는 멤버에 가야한다.
	for (int i = 0; i < MAX_PLAYER; ++i) {
		if (steamIDs[i] == 0) continue;

		shared_ptr<CPlayer> player = PlayerManager->GetPlayerById(steamIDs[i]);
		if (player) room = RoomManager->GetRoom(player);
		if (room) break;
	}
	if (room) {
		// 룸이 존재한다면 그 방에 들어간다.
		room->JoinRoom(player);
	}
	else {
		// 룸이 존재하지 않는다면 방을 새로 만든다.
		room = RoomManager->CreateRoom(player);
		if (bIsOnGame) RoomManager->ChangeRoomState(room, EGameState::GAME);
	}

	// 복구가 완료되었는지 확인한다.
	int playerCount = 0;
	for (int i = 0; i < MAX_PLAYER; ++i) {
		if (steamIDs[i] != 0) ++playerCount;
	}
	if (room->GetPlayerCount() == playerCount) {
		// 복구가 완료 되었다면 방장을 다시 맞추고 다시 플레이한다.
		for (int i = 1; i < MAX_PLAYER; ++i) {
			// 방장을 찾는다.
			auto innerRoomPlayer = room->GetPlayer(i);
			if (innerRoomPlayer && steamIDs[0] == innerRoomPlayer->steamID) {
				room->SwapPlayer(0, i);
				break;
			}
		}
		room->SendRoomInfoToAllMember();
	}

	WriteLog(ELogLevel::Warning, CLog::Format("[%s:%d] : C_RECONNECT_SERVER %llu\n", inet_ntoa(player->addr.sin_addr),
		ntohs(player->addr.sin_port), steamID));
}

auto ServerNetworkSystem = CServerNetworkSystem::GetInstance();
auto PlayerManager = CPlayerManager::GetInstance();
auto RoomManager = CRoomManager::GetInstance();