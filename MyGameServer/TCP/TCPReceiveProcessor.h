#pragma once
/*
	TCP 데이터 수신 로직만을 담은 클래스
*/

#include "MyGameServer.h"
#include <string>
#include "NetworkModule/Log.h"
#include "DataType/Overlapped.h"

class CPlayer;
class CTCPReceiveProcessor
{
public:
	static CTCPReceiveProcessor* GetInstance();

	// return false : ERROR
	bool ReceiveData(std::shared_ptr<CPlayer>& player, char* recvBuf, int receiveLen);

private:
	static CTCPReceiveProcessor* _instance;

	class CServerNetworkSystem* ServerNetworkSystem;
	class CPlayerManager* PlayerManager;
	class CRoomManager* RoomManager;

	void WriteLog(const ELogLevel& level, const std::string& msg);
	CTCPReceiveProcessor();
	~CTCPReceiveProcessor();

	// 메세지 처리부

	// Common
	void Common_Echo(std::shared_ptr<CPlayer>& player, char* recvBuf, int& cursor, int& bufLen);
	void Common_Ping(std::shared_ptr<CPlayer>& player, char* recvBuf, int& cursor, int& bufLen);
	void Common_AnswerId(std::shared_ptr<CPlayer>& player, char* recvBuf, int& cursor, int& bufLen);

	// Lobby
	void Lobby_InviteFriendRequest(std::shared_ptr<CPlayer>& player, char* recvBuf, int& cursor, int& bufLen);
	void Lobby_InviteFriendAnswer(std::shared_ptr<CPlayer>& player, char* recvBuf, int& cursor, int& bufLen);
	void Lobby_SetPartyKing(std::shared_ptr<CPlayer>& player, char* recvBuf, int& cursor, int& bufLen);
	void Lobby_FriendKickRequest(std::shared_ptr<CPlayer>& player, char* recvBuf, int& cursor, int& bufLen);
	void Lobby_MatchRequest(std::shared_ptr<CPlayer>& player, char* recvBuf, int& cursor, int& bufLen);

	// InGame
	void InGame_Spawn(std::shared_ptr<CPlayer>& player, char* recvBuf, int& cursor, int& bufLen);
	void InGame_EndGame(std::shared_ptr<CPlayer>& player, char* recvBuf, int& cursor, int& bufLen);
	void InGame_RPC(std::shared_ptr<CPlayer>& player, char* recvBuf, int& cursor, int& bufLen);
	void InGame_SyncVar(std::shared_ptr<CPlayer>& player, char* recvBuf, int& cursor, int& bufLen);
	void InGame_ReconnectServer(std::shared_ptr<CPlayer>& player, char* recvBuf, int& cursor, int& bufLen);

	// Debug
	void Debug_RoomStart(std::shared_ptr<CPlayer>& player, char* recvBuf, int& cursor, int& bufLen);
	void Debug_GameStart(std::shared_ptr<CPlayer>& player, char* recvBuf, int& cursor, int& bufLen);
};

