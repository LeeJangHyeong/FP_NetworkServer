#pragma once
/*
	프로젝트 전체를 개괄하는 헤더
*/

#include <string.h>
#ifdef _WIN32 // Window Socket
#include <WinSock2.h>

#elif __linux__ // Linux Socket
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#endif

// 디버깅용 플래그
//#define DEBUG_RECV_MSG
#define TCP_SERVER_PORT 15470
#define UDP_SERVER_PORT 15471
#define CHAR_TCP_SERVER_PORT "15470"
#define CHAR_UDP_SERVER_PORT "15471"
#define STEAM_APP_CODE 480

// 패킷 크기
#define PACKET_BUFSIZE 10000

// 잘려버린 패킷 저장 크기 (단일 메세지 최대 크기)
#define DELAYED_PACKET_BUFSIZE 5000

typedef unsigned long long int UINT64;