#pragma once
#include <memory>
#include "DataType/Player.h"
#include "DataType/Socket.h"

class IServer {
public:
	virtual bool Run() = 0;
	virtual void Send(const std::shared_ptr<CPlayer>& player, const char* buf, const int& sendLen, bool isTCP) = 0;
	virtual void Close(CSocket* socket) = 0;
	virtual void SendTo(FSocketAddrIn* sockaddr, const char* buf, const int& sendLen) = 0;
	virtual bool IsRun() = 0;
	virtual void Shutdown() = 0;
};