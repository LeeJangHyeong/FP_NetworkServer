#pragma once
#ifdef _WIN32
#include <WinSock2.h>
#include "DataType/Player.h"
#include "DataType/Socket.h"
#include "MyGameServer.h"
#include <memory>
#include <WinSock2.h>

class CPlayer;

enum class EOverlappedType
{
	ACCEPT,
	TCP_RECV,
	UDP_RECV,
	SEND,
	CLOSE
};

struct FOverlapped : public WSAOVERLAPPED
{
	FOverlapped(std::shared_ptr<CPlayer> player = nullptr)
	{
		ZeroMemory(this, sizeof(FOverlapped));
		m_player = player;
	}

	std::shared_ptr<CPlayer> m_player;
	EOverlappedType m_type;
};

struct FAcceptOverlapped : public FOverlapped
{
	struct _accept {
		BYTE m_pLocal[sizeof(FSocketAddrIn) + 16];
		BYTE m_pRemote[sizeof(FSocketAddrIn) + 16];
	} m_acceptBuf; // Accept에 사용하는 버퍼
	CSocket* socket; // Accept엔 Player를 담을 수 없으므로 소켓을 일단 저장한다.

	FAcceptOverlapped(std::shared_ptr<CPlayer> player = nullptr) : FOverlapped(player)
	{
		m_type = EOverlappedType::ACCEPT;
	}
};

struct FCloseOverlapped : public FOverlapped
{
	FCloseOverlapped(std::shared_ptr<CPlayer> player = nullptr) : FOverlapped(player)
	{
		m_type = EOverlappedType::CLOSE;
	}
	CSocket* socket;
};

struct FBuffuerableOverlapped : public FOverlapped
{
	FBuffuerableOverlapped(std::shared_ptr<CPlayer> player = nullptr) : FOverlapped(player)
	{
		wsabuf.buf = buf;
	}
	FBuffuerableOverlapped& operator = (const FBuffuerableOverlapped &rhs) {
		m_type = rhs.m_type;
		m_player = rhs.m_player;
		return *this;
	}
	char buf[PACKET_BUFSIZE];
	WSABUF wsabuf;
};

struct FTCPRecvOverlapped : public FBuffuerableOverlapped
{
	FTCPRecvOverlapped(std::shared_ptr<CPlayer> player = nullptr) : FBuffuerableOverlapped(player)
	{
		m_type = EOverlappedType::TCP_RECV;
	}
	FTCPRecvOverlapped& operator = (const FTCPRecvOverlapped &rhs) {
		m_type = rhs.m_type;
		m_player = rhs.m_player;
		return *this;
	}
};

struct FUDPRecvOverlapped : public FBuffuerableOverlapped
{
	FUDPRecvOverlapped(std::shared_ptr<CPlayer> player = nullptr) : FBuffuerableOverlapped(player)
	{
		m_type = EOverlappedType::UDP_RECV;
	}
	FUDPRecvOverlapped& operator = (const FUDPRecvOverlapped &rhs) {
		m_type = rhs.m_type;
		udpAddr = rhs.udpAddr;
		m_player = rhs.m_player;
		return *this;
	}
	FSocketAddrIn udpAddr;		// UDP에서 사용하는 ADDR
};

struct FSendOverlapped : public FBuffuerableOverlapped
{
	FSendOverlapped(std::shared_ptr<CPlayer> player = nullptr) : FBuffuerableOverlapped(player)
	{
		m_type = EOverlappedType::SEND;
	}
};
#endif