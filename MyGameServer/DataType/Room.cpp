#include "Room.h"
#include "Server/ServerNetworkSystem.h"
#include "UDP/UDPReceiveProcessor.h"
#include "NetworkModule/Serializer.h"
#include "Content/PlayerManager.h"
#include "Content/RoomManager.h"
#include "NetworkModule/MyTool.h"

using namespace std;
using namespace MyTool;
using namespace MySerializer;

typedef lock_guard<std::recursive_mutex> RecursiveLocker;

std::shared_ptr<CPlayer> CRoom::GetPlayer(int slot)
{
	if (slot < 0 || slot >= MAX_PLAYER) return nullptr;
	return players[slot];
}

void CRoom::SetPlayer(const int& slot, const std::shared_ptr<CPlayer>& player)
{
	if (slot < 0 || slot >= MAX_PLAYER) return;
	players[slot] = player;
}

void CRoom::SetState(const EGameState& newState)
{
	RecursiveLocker locker(mt_room);
	state = newState;
	for (int i = 0; i < MAX_PLAYER; ++i) {
		if (players[i]) {
			players[i]->state = state;
		}
	}
}

bool CRoom::ExistPlayer(const int& slot)
{
	if (slot < 0 || slot >= MAX_PLAYER) return false;
	return players[slot] != nullptr;
}

void CRoom::SwapPlayer(const int& slot1, const int& slot2)
{
	if (slot1 < 0 || slot1 >= MAX_PLAYER || slot2 < 0 || slot2 >= MAX_PLAYER || slot1 == slot2) return;

	mt_room.lock();
	shared_ptr<CPlayer> temp = players[slot1];
	players[slot1] = players[slot2];
	players[slot2] = temp;
	mt_room.unlock();
}

void CRoom::DisconnectPlayer(const std::shared_ptr<CPlayer>& player)
{
	if (!player) return;

	// 게임 중이라면 : Disconnect로만 처리하고, 마스터라면 다른 사람을 마스터로 만든다. 그리고 Disconnect를 통보한다.
	// 게임 중이 아니라면 : 같이 있는 파티에 null로 비워준다. 그리고 변경사항을 통보한다.

	mt_room.lock();
	if (state == GAME)
	{
		// 마스터라면? 1번 사람을 마스터로 바꾸어 버린다.
		if (players[0] == player) {
			SwapPlayer(0, 1);
		}
		for (int i = 0; i < MAX_PLAYER; ++i) {
			auto roomPlayer = players[i];
			if (roomPlayer == player) {
				roomPlayer->state = ConnectionLost;
				roomPlayer->udpAddr = nullptr;
				break;
			}
		}
		mt_room.unlock();
		SendRoomInfoToAllMember();
		SendDisconnectToAllMember(player);
	}
	else {
		for (int i = 0; i < MAX_PLAYER; ++i) {
			auto player_i = players[i];
			if (player_i == player) {
				// 뒷 슬롯을 당겨서 채운다.
				for (int j = i; j < MAX_PLAYER; ++j) {
					// 마지막 슬롯이거나, 다음 슬롯이 비었다면 현재 슬롯을 비운다.
					if (j == MAX_PLAYER - 1 || players[j + 1] == nullptr) players[j] = nullptr;
					// 아니라면 뒤에서 당겨온다.
					else players[j] = players[j + 1];
				}
				// 혹시라도 중복된 사람이 있을지 모르니, 다시 체크해보기위해 continue.
				--i;
				continue;
			}
		}

		// 나머지 인원들에게 변경 사항을 알려준다.
		mt_room.unlock();
		SendRoomInfoToAllMember();
		CRoomManager::GetInstance()->CheckAndRestateRoom(this->shared_from_this());
	}
}

bool CRoom::CanMatching()
{
	RecursiveLocker locker(mt_room);
	for (int i = 0; i < MAX_PLAYER; ++i) {
		if (players[i] && players[i]->state == LOBBY) {
			return false;
		}
	}
	return true;
}

bool CRoom::SetPlayerWhenEmpty(const int& slot, const std::shared_ptr<CPlayer>& player)
{
	if (slot < 0 || slot >= MAX_PLAYER) return false;
	RecursiveLocker locker(mt_room);
	if (!players[slot]) {
		players[slot] = player;
		return true;
	}
	return false;
}

void CRoom::ChangePartyKing(int targetSlot)
{
	if (targetSlot == 0) return;
	RecursiveLocker locker(mt_room);
	shared_ptr<CPlayer> temp = players[0];
	players[0] = players[targetSlot];
	players[targetSlot] = temp;
}

void CRoom::SendToAllMember(const char * buf, const int & len, const int& flag, bool isReliable)
{
	if (!players) return;
	RecursiveLocker locker(mt_room);
	for (int i = 0; i < MAX_PLAYER; ++i) {
		std::shared_ptr<CPlayer> player = players[i];
		if (player != nullptr && player->state != EGameState::ConnectionLost) {
			if (isReliable || player->udpAddr == nullptr) {
				CServerNetworkSystem::GetInstance()->Send(player, buf, len, true);
			}
			else {
				CServerNetworkSystem::GetInstance()->Send(player, buf, len, false);
			}
		}
	}
}

void CRoom::SendToOtherMember(const UINT64 member, const char * buf, const int & len, const int& flag, bool isReliable)
{
	if (!players) return;
	RecursiveLocker locker(mt_room);
	for (int i = 0; i < MAX_PLAYER; ++i) {
		std::shared_ptr<CPlayer> player = players[i];
		if (player != nullptr && player->steamID != member && player->state != EGameState::ConnectionLost) {
			if (isReliable || player->udpAddr == nullptr) {
				CServerNetworkSystem::GetInstance()->Send(player, buf, len, true);
			}
			else {
				CServerNetworkSystem::GetInstance()->Send(player, buf, len, false);
			}
		}

	}
}

void CRoom::SendRoomInfoToAllMember()
{
	WriteLog(ELogLevel::Warning, CLog::Format("[CRoom::SendRoomInfoToAllMember]\n"));

	mt_room.lock();
	// 방 멤버의 ID코드 MAX_PLAYER개를 담은 버퍼를 만든다.
	char allBuf[MAX_PLAYER * sizeof(UINT64) + sizeof(EMessageType)];
	int allLen = SerializeEnum(S_Room_Info, allBuf);
	for (int i = 0; i < MAX_PLAYER; ++i) {
		if (players[i] != nullptr) UInt64Serialize(allBuf + allLen, players[i]->steamID);
		else UInt64Serialize(allBuf + allLen, 0);
		allLen += (int)sizeof(UINT64);
	}
	mt_room.unlock();

	// 모든플레이어 들에게 버퍼를 보낸다.
	SendToAllMember(allBuf, allLen);
}

// 방의 모든 사람에게 해당 유저의 단절을 통보한다.
void CRoom::SendDisconnectToAllMember(const std::shared_ptr<CPlayer>& disconnectedMember)
{
	if (!disconnectedMember) return;

	WriteLog(ELogLevel::Warning, CLog::Format("[CRoom::SendDisconnectToAllMember]\n"));

	mt_room.lock();
	// 해당 멤버의 슬롯을 담아서 보낸다.
	char allBuf[sizeof(INT32) + sizeof(EMessageType)];
	int allLen = SerializeEnum(S_DISCONNECT_SLOT, allBuf);
	for (int i = 0; i < MAX_PLAYER; ++i) {
		if (players[i] == disconnectedMember) {
			IntSerialize(allBuf + allLen, i);
			allLen += (int)sizeof(INT32);
			break;
		}
	}
	mt_room.unlock();

	// 다른 플레이어 들에게 버퍼를 보낸다.
	SendToOtherMember(disconnectedMember->steamID, allBuf, allLen);
}

// 방의 모든 사람에게 해당 유저의 재접속을 통보한다.
void CRoom::SendReconnectToAllMember(const std::shared_ptr<CPlayer>& reconnectedMember)
{
	// 방의 기존 유저들에게 해당 유저의 슬롯을 보낸다.
	{
		WriteLog(ELogLevel::Warning, CLog::Format("[CRoom::SendReconnectToAllMember]\n"));

		mt_room.lock();
		// 해당 멤버의 슬롯을 담아서 보낸다.
		char allBuf[sizeof(INT32) + sizeof(EMessageType)];
		int allLen = SerializeEnum(S_RECONNECT_SLOT, allBuf);
		for (int i = 0; i < MAX_PLAYER; ++i) {
			if (players[i] == reconnectedMember) {
				IntSerialize(allBuf + allLen, i);
				allLen += (int)sizeof(INT32);
				break;
			}
		}
		mt_room.unlock();

		// 다른 플레이어 들에게 버퍼를 보낸다.
		SendToOtherMember(reconnectedMember->steamID, allBuf, allLen);
	}

	// 나간사람에게는 재접속을 통보하고 방의 정보를 다시 갱신해준다.
	{
		mt_room.lock();
		// 방 멤버의 ID코드 MAX_PLAYER개를 담은 버퍼를 만든다.
		char allBuf[MAX_PLAYER * sizeof(UINT64) + sizeof(EMessageType)];
		int allLen = SerializeEnum(S_RECONNECT, allBuf);
		for (int i = 0; i < MAX_PLAYER; ++i) {
			if (players[i] != nullptr) UInt64Serialize(allBuf + allLen, players[i]->steamID);
			else UInt64Serialize(allBuf + allLen, 0);
			allLen += (int)sizeof(UINT64);
		}

		// 재접속한 유저에게 방의 정보를 통보해준다.
		CServerNetworkSystem::GetInstance()->Send(reconnectedMember, allBuf, allLen, true);
		mt_room.unlock();
	}
}

void CRoom::JoinRoom(const shared_ptr<CPlayer>& player)
{
	if (GetPlayerCount() >= MAX_PLAYER) return;
	RecursiveLocker locker(mt_room);
	for (int i = 0; i < MAX_PLAYER; ++i) {
		if (players[i] == nullptr) {
			players[i] = player;
			break;
		}
	}
}

int CRoom::GetPlayerCount()
{
	int count = 0;
	for (int i = 0; i < MAX_PLAYER; ++i) {
		if (players[i] != nullptr) ++count;
	}
	return count;
}

int CRoom::GetNotLostedPlayerCount()
{
	int count = 0;
	for (int i = 0; i < MAX_PLAYER; ++i) {
		auto player = players[i];
		if (player && player->state != ConnectionLost) ++count;
	}
	return count;
}

void CRoom::DeletePlayer(const int& slot, bool bWithPushing)
{
	if (slot < 0 || slot >= MAX_PLAYER) return;
	RecursiveLocker locker(mt_room);

	if (bWithPushing) {
		// 뒷 슬롯을 당겨서 채운다.
		for (int i = slot; i < MAX_PLAYER; ++i) {
			// 마지막 슬롯이거나, 다음 슬롯이 비었다면 현재 슬롯을 비운다.
			if (i == MAX_PLAYER - 1 || players[i + 1] == nullptr) players[i] = nullptr;
			// 아니라면 뒤에서 당겨온다.
			else players[i] = players[i + 1];
		}
	}
	else players[slot] = nullptr;
}

bool CRoom::FindPlayer(const std::shared_ptr<CPlayer>& player)
{
	for (int i = 0; i < MAX_PLAYER; ++i) {
		if (players[i] == player) return true;
	}
	return false;
}

void CRoom::WriteLog(const ELogLevel& level, const std::string& msg)
{
	CLog::WriteLog(ELogType::Room, level, msg);
#ifdef DEBUG_RECV_MSG
	printf("%s\n", msg.c_str());
#endif
}