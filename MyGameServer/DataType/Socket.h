#pragma once
#include "MyGameServer.h"
#include "NetworkModule/Log.h"

#ifdef _WIN32 // Window Socket
typedef SOCKADDR FSocketAddr;
typedef SOCKADDR_IN FSocketAddrIn;

#elif __linux__ // Linux Socket
typedef sockaddr FSocketAddr;
typedef sockaddr_in FSocketAddrIn;
typedef int SOCKET;
#endif

class CSocket
{
public:
	CSocket();
	~CSocket();
	CSocket(SOCKET socket);
	operator SOCKET()
	{
		return _socket;
	}

	SOCKET GetSocket();
private:
	SOCKET _socket;
	void _InitSocket();
	static void WriteLog(const ELogLevel& level, const std::string& msg);
};