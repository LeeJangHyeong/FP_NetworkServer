#pragma once
#include "MyGameServer.h"
#include "DataType/GameState.h"
#include "DataType/Socket.h"
#include <ctime>
#include <memory>
#include <chrono>
#include <ctime>
#include <mutex>
#include "Overlapped.h"

typedef std::chrono::time_point<std::chrono::system_clock> TIME;
struct FTCPRecvOverlapped;
struct FUDPRecvOverlapped;
struct FBuffuerableOverlapped;

class CPlayer
{
public:
	UINT64 steamID;
	CSocket* socket = nullptr;
	FSocketAddrIn addr;
	std::shared_ptr<FSocketAddrIn> udpAddr = nullptr;
	TIME lastPingTime;
	TIME lastPongTime;
	EGameState state;

	// 누락된 데이터 저장버퍼
	char delayedTCPData[DELAYED_PACKET_BUFSIZE];
	char delayedUDPData[DELAYED_PACKET_BUFSIZE];
	// 누락된 데이터 크기
	int delayedTCPDataLen = 0;
	int delayedUDPDataLen = 0;
	CPlayer();
	~CPlayer();

	// 남은 버퍼가 있는지 확인하고 있다면 꺼내서 현재 버퍼에 합쳐준다.
	// 리턴값은 새로 늘어난 길이.
	int PopDelayData(char* buf, int& ref_len, bool isTCP);

	// 남은 데이터를 delayedData에 넣어준다.
	void PushDelayData(char* buf, const int& cursor, const int& recvLen, bool isTCP);

private:
	std::mutex mt_player;
};