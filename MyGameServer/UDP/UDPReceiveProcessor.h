#pragma once
/*
	레플리케이션 시스템(SyncVar, RPC)에서 신뢰성을 포기하고 속도를 갖도록 하기위한 UDP 프로토콜의 수신 담당하는 클래스.
*/

#include "MyGameServer.h"
#include <string>
#include "NetworkModule/Log.h"
#include "DataType/Overlapped.h"
#include "DataType/Socket.h"

class CPlayer;

class CUDPReceiveProcessor
{
public:
	static CUDPReceiveProcessor* GetInstance();

	// return false : ERROR
	bool ReceiveData(const FSocketAddrIn& addr, char* buf, int receiveLen);
private:
	static CUDPReceiveProcessor* _instance;

	class CServerNetworkSystem* ServerNetworkSystem;
	class CPlayerManager* PlayerManager;
	class CRoomManager* RoomManager;

	void WriteLog(ELogLevel level, std::string msg);
	CUDPReceiveProcessor();
	~CUDPReceiveProcessor() {}

	// 메세지 처리부

	void HeatBeat(std::shared_ptr<CPlayer>& player, char* recvBuf, int& cursor, int& bufLen, const FSocketAddrIn& clientAddr);
	void UDPReg(std::shared_ptr<CPlayer>& player, char* recvBuf, int& cursor, int& bufLen, const FSocketAddrIn& clientAddr);
	
	// InGame
	void InGame_RPC(std::shared_ptr<CPlayer>& player, char* recvBuf, int& cursor, int& bufLen);
	void InGame_SyncVar(std::shared_ptr<CPlayer>& player, char* recvBuf, int& cursor, int& bufLen);
};