#define  _WINSOCK_DEPRECATED_NO_WARNINGS
#include "UDPReceiveProcessor.h"
#include "Server/ServerNetworkSystem.h"
#include "Content/PlayerManager.h"
#include "Content/RoomManager.h"
#include "DataType/Room.h"
#include "DataType/Player.h"
#include "NetworkModule/NetworkData.h"
#include "NetworkModule/Serializer.h"
#include "NetworkModule/Log.h"
#include "NetworkModule/MyTool.h"
#include <stdio.h>
#include <chrono>
#include <ctime> 
#include <memory>

using namespace MyTool;
using namespace std;
using namespace MySerializer;

typedef lock_guard<mutex> Lock;

CUDPReceiveProcessor* CUDPReceiveProcessor::_instance = nullptr;

CUDPReceiveProcessor* CUDPReceiveProcessor::GetInstance()
{
	if (_instance == nullptr) _instance = new CUDPReceiveProcessor();
	return _instance;
}

bool CUDPReceiveProcessor::ReceiveData(const FSocketAddrIn& addr, char* buf, int receiveLen)
{
	if (receiveLen < 0 || receiveLen > PACKET_BUFSIZE) return false;
	if (!ServerNetworkSystem || !PlayerManager || !RoomManager) {
		std::cout << "[CUDPReceiveProcessor::ReceiveData] Critical Error!!!!\n";
		return false;
	}

	auto player = PlayerManager->GetPlayerByUDPAddr(addr);
	// 남은 버퍼가 있는지 확인하고 있다면 꺼내서 현재 버퍼에 합쳐준다.
	if (player && player->delayedUDPData > 0) {
		player->PopDelayData(buf, receiveLen, false);
	}

	int cursor = 0, bufLen = 0;
	EMessageType type;
	char* recvBuf = buf;
	while (cursor < receiveLen) {
		// 데이터 처리
		bufLen = IntDeserialize(recvBuf, &cursor) - (int)sizeof(EMessageType);
		if (bufLen < 0) {
			WriteLog(ELogLevel::Error, CLog::Format("UDP Receive buflen < 0"));
			return false;
		}
		if (bufLen > PACKET_BUFSIZE) {
			WriteLog(ELogLevel::Error, CLog::Format("UDP Receive buflen > BUFSIZE, bufSize = %d", bufLen));
			return false;
		}

		// 수신량을 넘어서 데이터가 넘어온다면 남은 데이터를 저장하고 리턴한다.
		if (cursor + bufLen + sizeof(EMessageType) > receiveLen) {
			if (!player) return true;
			cursor -= (int)sizeof(INT32);
			player->PushDelayData(recvBuf, cursor, receiveLen, false);
			return true;
		}

		type = GetEnum(recvBuf, &cursor);
		switch (type)
		{
		case UDP_HEART_BEAT:
		{
			HeatBeat(player, recvBuf, cursor, bufLen, addr);
			break;
		}
		case C_UDP_Reg:
		{
			UDPReg(player, recvBuf, cursor, bufLen, addr);
			break;
		}
		case C_INGAME_RPC:
		{
			InGame_RPC(player, recvBuf, cursor, bufLen);
			break;
		}
		case C_INGAME_SyncVar:
		{
			InGame_SyncVar(player, recvBuf, cursor, bufLen);
			break;
		}
		default:
		{
			WriteLog(ELogLevel::Error, "Unknown UDP Type");

			cursor += bufLen;
			break;
		}
		}
	}
	return true;
}

void CUDPReceiveProcessor::WriteLog(ELogLevel level, std::string msg)
{
	CLog::WriteLog(ELogType::UDPReceiveProcessor, level, msg);
#ifdef DEBUG_RECV_MSG
	printf("%s\n", msg.c_str());
#endif // DEBUG_RECV_MSG
}

CUDPReceiveProcessor::CUDPReceiveProcessor()
{
	ServerNetworkSystem = CServerNetworkSystem::GetInstance();
	PlayerManager = CPlayerManager::GetInstance();
	RoomManager = CRoomManager::GetInstance();
}

void CUDPReceiveProcessor::HeatBeat(std::shared_ptr<CPlayer>& player, char* recvBuf, int& cursor, int& bufLen, const FSocketAddrIn& clientAddr)
{
	char buf[sizeof(EMessageType)];
	int len = SerializeWithEnum(EMessageType::UDP_HEART_BEAT, nullptr, 0, buf);
	ServerNetworkSystem->SendTo(const_cast<FSocketAddrIn*>(&clientAddr), buf, len);
}

void CUDPReceiveProcessor::UDPReg(std::shared_ptr<CPlayer>& player, char* recvBuf, int& cursor, int& bufLen, const FSocketAddrIn& clientAddr)
{
	UINT64 steamId = UInt64Deserialize(recvBuf, &cursor);

	if (steamId == 0) return;

	// 이미 연결되었다면? 리턴.
	if (player) return;

	WriteLog(ELogLevel::Warning, CLog::Format(" [STEAM: %llu ] C_UDP_Reg  : try to reg\n", steamId));

	// 해당 TCPAddr에 UDP에 연결되지 않은 유저를 찾는다.
	player = PlayerManager->GetPlayerById(steamId);
	if (!player) {
		WriteLog(ELogLevel::Warning, CLog::Format("[ STEAM: %llu ] C_UDP_Reg  : not exist in tcp player...\n", steamId));
		return;
	}

	FSocketAddrIn clientSockAddr = FSocketAddrIn(clientAddr);
	player->udpAddr = std::make_shared<FSocketAddrIn>(clientSockAddr);

	char responseBuf[sizeof(EMessageType)];
	SerializeEnum(EMessageType::S_UDP_Response, responseBuf);
	ServerNetworkSystem->Send(player, responseBuf, sizeof(EMessageType), false);
	WriteLog(ELogLevel::Warning, CLog::Format("[ STEAM: %llu ] C_UDP_Reg  : SUCESS!\n", steamId));
}

void CUDPReceiveProcessor::InGame_RPC(std::shared_ptr<CPlayer>& player, char* recvBuf, int& cursor, int& bufLen)
{
	// Get Player's Socket Info.
	if (!player) {
		WriteLog(ELogLevel::Warning, CLog::Format("C_INGAME_RPC  : not exist player...\n"));
		cursor += bufLen;
		return;
	}
	char type = CharDeserialize(recvBuf, &cursor);
	bufLen -= 1; // 캐릭터 바이트 사이즈 만큼 뺀다.
	std::shared_ptr<CRoom> targetRoom = RoomManager->GetGameRoom(player);
	if (!targetRoom || targetRoom->GetState() != GAME) {
		cursor += bufLen;
		return;
	}
	switch (type)
	{
		case 0:
		{
			// 멀티캐스트
			std::shared_ptr<char> pNewBuf(new char[bufLen + sizeof(EMessageType)]);

			SerializeEnum(S_INGAME_RPC, (char*)pNewBuf.get());
			memcpy((char*)pNewBuf.get() + (int)sizeof(EMessageType), recvBuf + cursor, bufLen);
			targetRoom->SendToOtherMember(player->steamID, (char*)pNewBuf.get(), bufLen + (int)sizeof(EMessageType), 0, false);
			break;
		}
		case 1:
		{
			// 마스터에서 처리.
			// 송신자가 마스터라면 무시한다.
			auto masterPlayer = targetRoom->GetPlayer(0);
			if (masterPlayer == player) break;

			// 마스터에게 전달
			std::shared_ptr<char> pNewBuf(new char[bufLen + sizeof(EMessageType)]);

			SerializeEnum(S_INGAME_RPC, (char*)pNewBuf.get());
			memcpy(pNewBuf.get() + (int)sizeof(EMessageType), recvBuf + cursor, bufLen);
			if (masterPlayer->udpAddr == nullptr)
				ServerNetworkSystem->Send(masterPlayer, pNewBuf.get(), bufLen + (int)sizeof(EMessageType), true);
			else ServerNetworkSystem->Send(masterPlayer, pNewBuf.get(), bufLen + (int)sizeof(EMessageType), false);

			break;
		}
		default:
			WriteLog(ELogLevel::Error, CLog::Format("[%s:%d] : Unknown RPC type.\n", inet_ntoa(player->addr.sin_addr),
				ntohs(player->addr.sin_port)));
			break;
	}
	cursor += bufLen;
}

void CUDPReceiveProcessor::InGame_SyncVar(std::shared_ptr<CPlayer>& player, char* recvBuf, int& cursor, int& bufLen)
{
	// Get Player's Socket Info.
	if (!player) {
		WriteLog(ELogLevel::Warning, CLog::Format("C_INGAME_SyncVar  : not exist player...\n"));
		cursor += bufLen;
		return;
	}
	std::shared_ptr<CRoom> targetRoom = RoomManager->GetGameRoom(player);
	if (!targetRoom || targetRoom->GetState() != GAME) {
		cursor += bufLen;
		return;
	}
	// 본인을 제외한 나머지 파티원에게 전달.
	std::shared_ptr<char> pNewBuf(new char[bufLen + sizeof(EMessageType)]);

	SerializeEnum(S_INGAME_SyncVar, pNewBuf.get());
	memcpy(pNewBuf.get() + sizeof(EMessageType), recvBuf + cursor, bufLen);
	targetRoom->SendToOtherMember(player->steamID, pNewBuf.get(), bufLen + (int)sizeof(EMessageType), 0, false);

	cursor += bufLen;
}
