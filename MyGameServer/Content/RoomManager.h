#pragma once
/*
	방에 대한 내용을 관할하는 클래스
*/

#define MAX_PLAYER 2
#include "MyGameServer.h"
#include "NetworkModule/NetworkData.h"
#include "PlayerManager.h"
#include <list>
#include <set>
#include <mutex>
#include <memory>

using std::list;
using std::mutex;
using std::set;
class CRoom;

const UINT64 MAX_ROOMCOUNT = 18446744073709551;
class CRoomManager
{
public:
	static CRoomManager* GetInstance();
	~CRoomManager();

	// innerMember의 룸을 찾아서, outterMember를 넣어준다.
	// 룸은 자동으로 나가짐.
	// 리턴값은 성공 여부.
	bool MoveRoom(const std::shared_ptr<CPlayer>& innerMember, const std::shared_ptr<CPlayer>& outterMember, int& outSlotNumber);

	// 새로운 방을 만든다.
	// (새로운 유저가 접속했을때)
	std::shared_ptr<CRoom> CreateRoom(const std::shared_ptr<CPlayer> member);

	// 플레이어를 추방시킨다음 새로운 방에 넣어준다.
	void KickPlayer(const std::shared_ptr<CPlayer>& targetPlayer);

	std::shared_ptr<CRoom> GetRoom(const std::shared_ptr<CPlayer>& innerMember);

	// 연결상태인 룸을 찾는다.
	std::shared_ptr<CRoom> GetConnectedRoom(const std::shared_ptr<CPlayer>& innerMember);

	// Room을 Ready 상태로 바꿉니다.
	void ChangeRoomReady(const std::shared_ptr<CPlayer>& player, const bool& isOn);

	int GetPlayerCount();
	int GetMatchRoomCount();
	int GetGameRoomCount();

	// 룸에서 단절된 플레이어를 알린다.
	void DisconnectRoom(const std::shared_ptr<CPlayer>& player);

	// 한 ServerNetworkSystem Process Thread에 한번씩 호출되는 함수.
	void Update();

	// 강제로 룸에 조인시켜준다. DEBUG ONLY
	void ForceJoinRoom(const std::shared_ptr<CPlayer>& targetPlayer);

	// 강제로 게임중인 룸에 조인시켜준다. DEBUG ONLY
	bool ForceJoinGameRoom(const std::shared_ptr<CPlayer>& targetPlayer);

	// 강제로 방의 상태를 바꾼다. DEBUG ONLY
	void ForceChangeGameState(const std::shared_ptr<CRoom>& room);
	std::shared_ptr<CRoom> GetGameRoom(const std::shared_ptr<CPlayer>& player);
	
	// 모든 방에서 나가고,
	// 이를 모든 클라이언트에게 알린다.
	// * 위험한 행동임.
	void OutRoom(const std::shared_ptr<CPlayer>& innerMember);

	// 룸의 스테이트를 바꾼다.
	// 스테이트가 바뀌면서 리스트도 재정리 된다.
	void ChangeRoomState(const std::shared_ptr<CRoom>& room, const EGameState& state);

	// 룸의 상태를 확인한다.
	void CheckAndRestateRoom(const std::shared_ptr<CRoom>& room);

private:
	CRoomManager();

	int GetRoomMemberCount(const std::shared_ptr<CPlayer>& innerMember);
	int GetRoomMemberCount(const std::shared_ptr<CRoom>& room);

	void DeleteRoom(const UINT64& roomNumb);

	// 플레이 가능한 다른 방을 찾아본다.
	// * 제귀함수이므로 Lock 보장을 하지않으니 상위에서 해야함.
	bool TryMatching(const int& memberCount, set<std::shared_ptr<CRoom>>& outRooms);

	UINT64 roomCounter;
	list<std::shared_ptr<CRoom>> rooms;
	list<std::shared_ptr<CRoom>> matchRooms;
	list<std::shared_ptr<CRoom>> gameRooms;

	static CRoomManager* _instance;

	static void WriteLog(const ELogLevel& level, const std::string& msg);

	// 방 이동을 수행한다.
	// 성공시 true 리턴
	bool _MoveRoomProc(const std::shared_ptr<CPlayer>& innerMember, const std::shared_ptr<CPlayer>& outterMember, int& outSlotNumber);

	// 비동기를 지키기위한 뮤텍스.
	std::recursive_mutex mt_roomManager;
};