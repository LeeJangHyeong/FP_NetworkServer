#include "RoomManager.h"
#include "DataType/Room.h"
#include "NetworkModule/Serializer.h"
#include "Server/ServerNetworkSystem.h"
#include "NetworkModule/MyTool.h"
#include "NetworkModule/Log.h"
#include <string.h>
#include <algorithm>

using namespace std;
using namespace MyTool;
using namespace MySerializer;

typedef lock_guard<std::recursive_mutex> RecursiveLock;

CRoomManager* CRoomManager::_instance = nullptr;

void CRoomManager::WriteLog(const ELogLevel& level, const std::string& msg)
{
	CLog::WriteLog(ELogType::RoomManager, level, msg);
#ifdef DEBUG_RECV_MSG
	printf("%s\n", msg.c_str());
#endif
}

bool CRoomManager::_MoveRoomProc(const std::shared_ptr<CPlayer>& innerMember, const std::shared_ptr<CPlayer>& outterMember, int& outSlotNumber)
{
	if (innerMember.get() == nullptr) return false;

	// 접속자가 이미 파티중이라면 이동하지 않는다.
	std::shared_ptr<CRoom> outterRoom = GetRoom(outterMember);
	if (!outterRoom && GetRoomMemberCount(outterRoom) != 1) return false;

	std::shared_ptr<CRoom> room = GetRoom(innerMember);
	if (GetRoomMemberCount(room) < MAX_PLAYER) {
		// 방 접속 가능
		// 기존방에서 나간다.
		OutRoom(outterMember);
		for (int i = 0; i < MAX_PLAYER; ++i) {
			if (room->SetPlayerWhenEmpty(i, outterMember)) {
				outSlotNumber = i;

				// 방접속 성공!
				return true;
			}
		}
		// 방 접속에 실패했다면, 방을 다시 만들어준다.
		CreateRoom(outterMember);
	}

	// 방 접속 실패 !!!
	return false;
}

CRoomManager::CRoomManager()
{
	roomCounter = 1;
}


CRoomManager * CRoomManager::GetInstance()
{
	if (!_instance) _instance = new CRoomManager();
	return _instance;
}

CRoomManager::~CRoomManager()
{
}

std::shared_ptr<CRoom> CRoomManager::CreateRoom(const shared_ptr<CPlayer> member)
{
	std::shared_ptr<CRoom> newRoom(new CRoom(member, roomCounter++));
	if (roomCounter == MAX_ROOMCOUNT) roomCounter = 1;
	
	mt_roomManager.lock();
	rooms.push_back(newRoom);
	mt_roomManager.unlock();

	return newRoom;
}

void CRoomManager::KickPlayer(const std::shared_ptr<CPlayer>& targetPlayer)
{
	if (!targetPlayer) return;

	OutRoom(targetPlayer);
	CreateRoom(targetPlayer);
	std::shared_ptr<CRoom> kickPlayerRoom = GetRoom(targetPlayer);

	// 강퇴 된 이후의 방 정보를 알려준다.
	if (kickPlayerRoom) kickPlayerRoom->SendRoomInfoToAllMember();
}

shared_ptr<CRoom> CRoomManager::GetRoom(const shared_ptr<CPlayer>& innerMember)
{
	if (innerMember == nullptr) return nullptr;
	RecursiveLock locker(mt_roomManager);
	for (auto i = rooms.begin(); i != rooms.end(); ++i) {
		for (int j = 0; j < MAX_PLAYER; ++j) {
			if ((*i)->GetPlayer(j) == innerMember) {
				return *i;
			}
		}
	}
	return nullptr;
}

std::shared_ptr<CRoom> CRoomManager::GetConnectedRoom(const std::shared_ptr<CPlayer>& innerMember)
{
	if (innerMember == nullptr) return nullptr;
	RecursiveLock locker(mt_roomManager);
	for (auto i = rooms.begin(); i != rooms.end(); ++i) {
		for (int j = 0; j < MAX_PLAYER; ++j) {
			auto player = (*i)->GetPlayer(j);
			if (player && player == innerMember && player->state != EGameState::ConnectionLost) {
				return *i;
			}
		}
	}
	return nullptr;
}

void CRoomManager::DeleteRoom(const UINT64& roomNumb)
{
	RecursiveLock locker(mt_roomManager);
	for (list<std::shared_ptr<CRoom>>::iterator it = rooms.begin();
		it != rooms.end();)
	{
		if ((*it)->GetRoomNumber() == roomNumb) {
			std::shared_ptr<CRoom> ptr = *it;
			rooms.remove(ptr);
			matchRooms.remove(ptr);
			gameRooms.remove(ptr);
			break;
		}
		else
			++it;
	}
}

void CRoomManager::ChangeRoomState(const std::shared_ptr<CRoom>& room, const EGameState& state)
{
	if (!room) return;
	mt_roomManager.lock();
	if (room->GetState() == state) {
		mt_roomManager.unlock();
		return;
	}
	switch (state)
	{
	case LOBBY:
		if (room->GetState() == READY) {
			matchRooms.remove(room);
		}
		else if (room->GetState() == GAME) {
			gameRooms.remove(room);
		}
		break;
	case READY:
			matchRooms.push_back(room);
		break;
	case GAME:
			matchRooms.remove(room);
			gameRooms.push_back(room);
		break;
	default:
		break;
	}
	room->SetState(state);
	mt_roomManager.unlock();
}

void CRoomManager::CheckAndRestateRoom(const std::shared_ptr<CRoom>& room){
	if (!room) return;
	// 로비/매칭 레디 상태에선 상호 교환이 일어난다.
	if (room->GetState() == LOBBY || room->GetState() == READY) {
		if (room->CanMatching()) {
			ChangeRoomState(room, READY);
		}
		else {
			ChangeRoomState(room, LOBBY);
		}
	}
}

std::shared_ptr<CRoom> CRoomManager::GetGameRoom(const shared_ptr<CPlayer>& player)
{
	std::shared_ptr<CRoom> playerRoom = GetRoom(player);
	if (playerRoom == nullptr) return nullptr;

	auto it = std::find(gameRooms.begin(), gameRooms.end(), playerRoom);
	if (it != gameRooms.end()) { // founded!!
		return playerRoom;
	}
	else { // not founded.
		return nullptr;
	}
}

bool CRoomManager::TryMatching(const int& memberCount, set<std::shared_ptr<CRoom>>& outRooms)
{
	if (memberCount == MAX_PLAYER) return true;
	for (auto i : matchRooms) {
		// 이미 집합에 존재하는 멤버면 무시한다.
		if (outRooms.find(i) != outRooms.end()) continue;

		// 플레이어 카운트를 합산해 다시 계산.
		int count = i->GetPlayerCount();

		if (count > 0 && count + memberCount <= MAX_PLAYER) {
			outRooms.insert(i);
			if(TryMatching(memberCount + count, outRooms)) return true;
			outRooms.erase(i);
		}
	}
	return false;
}

void CRoomManager::ChangeRoomReady(const shared_ptr<CPlayer>& player, const bool& isOn)
{
	std::shared_ptr<CRoom> room = GetRoom(player);
	if (!room) return;
	if (room->GetState() == GAME) return;

	mt_roomManager.lock();
	for (int i = 0; i < MAX_PLAYER; ++i) {
		// 플레이어를 찾음
		if (room->GetPlayer(i) == player) {
			if(isOn) player->state = READY;
			else player->state = LOBBY;

			char buf[sizeof(EMessageType) + sizeof(bool) + sizeof(int)];
			SerializeEnum(S_Lobby_MatchAnswer, buf);
			BoolSerialize(buf + sizeof(EMessageType), isOn);
			IntSerialize(buf + sizeof(EMessageType) + sizeof(bool), i);

			// 갱신 정보를 전송
			for (int j = 0; j < MAX_PLAYER; ++j) {
				auto player_j = room->GetPlayer(j);
				if (player_j != nullptr) {
					CServerNetworkSystem::GetInstance()->Send(player_j, buf,
						(int)(sizeof(EMessageType) + sizeof(bool) + sizeof(int)), true);
				}
			}

			mt_roomManager.unlock();
			CheckAndRestateRoom(room);
			return;
		}
	}
	mt_roomManager.unlock();
}

int CRoomManager::GetPlayerCount() {
	return static_cast<int>(rooms.size());
}

int CRoomManager::GetMatchRoomCount()
{
	return static_cast<int>(matchRooms.size());
}

int CRoomManager::GetGameRoomCount()
{
	return static_cast<int>(gameRooms.size());
}

void CRoomManager::DisconnectRoom(const shared_ptr<CPlayer>& player)
{
	std::shared_ptr<CRoom> room = GetConnectedRoom(player);
	// 회원이 접속중인 룸을 조회
	if (room != nullptr) {
		mt_roomManager.lock();
		int count = room->GetNotLostedPlayerCount();

		// 혼자 있는 방은 삭제
		if (count <= 1) {
			mt_roomManager.unlock();
			DeleteRoom(room->GetRoomNumber());

			// 플레이어도 내쫒는다.
			for (int j = 0; j < MAX_PLAYER; ++j) {
				auto player_j = room->GetPlayer(j);
				if (player_j) {
					CPlayerManager::GetInstance()->RemovePlayer(player_j);
					room->DeletePlayer(j);
				}
			}
			mt_roomManager.lock();
		}
		else {
			mt_roomManager.unlock();
			room->DisconnectPlayer(player);
			mt_roomManager.lock();

			// 혹시라도 룸이 비어버렸다면 다시한번 삭제처리를 한다.
			if (room->GetNotLostedPlayerCount() == 0) {
				mt_roomManager.unlock();
				DeleteRoom(room->GetRoomNumber());
				mt_roomManager.lock();
			}
		}
		mt_roomManager.unlock();
	}
}

void CRoomManager::Update()
{
	// 매칭을 시켜준다.
	set<std::shared_ptr<CRoom>> matchingSuccessRooms;
	mt_roomManager.lock();
	for (auto i : matchRooms) {
		matchingSuccessRooms.insert(i);
		int count = i->GetPlayerCount();
		if (TryMatching(count, matchingSuccessRooms)) {
			matchingSuccessRooms.erase(i);
			// i의 룸으로 모두 이전 시킨다.
			int emptySlot = 0;
			for (int i_slot = 0; i_slot < MAX_PLAYER; ++i_slot) {
				if (!i->ExistPlayer(i_slot)) {
					emptySlot = i_slot;
					break;
				}
			}
			// 빈곳을 집합에서 한명씩 넣어준다.
			for (auto matchedRoom : matchingSuccessRooms) {
				for (int matchedRoom_slot = 0; matchedRoom_slot < MAX_PLAYER; ++matchedRoom_slot) {
					// 매칭된 룸에서 사람이 존재한다면 이동.
					if (matchedRoom->ExistPlayer(matchedRoom_slot)) {
						if(emptySlot<MAX_PLAYER) i->SetPlayer(emptySlot++, matchedRoom->GetPlayer(matchedRoom_slot));
						matchedRoom->DeletePlayer(matchedRoom_slot);
					}
				}
				// 이후 매칭된 방을 파괴시킨다.
				rooms.remove(matchedRoom);
				matchRooms.remove(matchedRoom);
				gameRooms.remove(matchedRoom);
			}

			// 룸의 상태를 갱신시켜주고, 게임 시작을 알린다.
			WriteLog(ELogLevel::Warning, CLog::Format("[%llu room] Game Start!\n", i->GetRoomNumber()));
			ChangeRoomState(i, GAME);
			i->SendRoomInfoToAllMember();
			char buf[sizeof(EMessageType)];
			SerializeEnum(S_Lobby_GameStart, buf);
			i->SendToAllMember(buf, sizeof(EMessageType));

			// 성공했다면 처음부터 다시 진행한다.
			mt_roomManager.unlock();
			Update();
			return;
		}
		matchingSuccessRooms.clear();
	}
	// 빈 방을 삭제시켜준다.
	auto it = gameRooms.begin();
	while (it != gameRooms.end())
	{
		bool isLost = true;
		for (int j = 0; j < MAX_PLAYER; ++j) {
			auto player_j = (*it)->GetPlayer(j);
			if (player_j && player_j->state != EGameState::ConnectionLost) {
				isLost = false;
				break;
			}
		}

		if (isLost) {
			for (int j = 0; j < MAX_PLAYER; ++j) {
				auto player_j = (*it)->GetPlayer(j);
				if (player_j) {
					CPlayerManager::GetInstance()->RemovePlayer(player_j);
					(*it)->DeletePlayer(j);
				}
			}
			std::shared_ptr<CRoom> ptr = *it;
			rooms.remove(ptr);
			matchRooms.remove(ptr);
			gameRooms.remove(ptr);
			it++;
		}
		else it++;
	}
	mt_roomManager.unlock();
}

void CRoomManager::ForceJoinRoom(const shared_ptr<CPlayer>& targetPlayer)
{
	mt_roomManager.lock();
	for (auto i : rooms) {
		int count = i->GetPlayerCount();
		if (count < MAX_PLAYER) {
			std::shared_ptr<CRoom>  room = i;

			// 이미 속한 방이 아니라면 접속하고 통보.
			if (room->FindPlayer(targetPlayer)) continue;

			mt_roomManager.unlock();
			OutRoom(targetPlayer);
			room->JoinRoom(targetPlayer);
			room->SendRoomInfoToAllMember();
			return;
		}
	}
	mt_roomManager.unlock();
}

bool CRoomManager::ForceJoinGameRoom(const shared_ptr<CPlayer>& targetPlayer)
{
	mt_roomManager.lock();
	for (auto i : gameRooms) {
		int count = i->GetPlayerCount();
		if (count < MAX_PLAYER) {
			std::shared_ptr<CRoom>  room = i;

			// 이미 속한 방이 아니라면 접속하고 통보.
			if (room->FindPlayer(targetPlayer)) continue;

			mt_roomManager.unlock();
			OutRoom(targetPlayer);
			room->JoinRoom(targetPlayer);
			room->SendRoomInfoToAllMember();
			targetPlayer->state = GAME;
			return true;
		}
	}
	mt_roomManager.unlock();
	return false;
}

void CRoomManager::ForceChangeGameState(const std::shared_ptr<CRoom>& room)
{
	ChangeRoomState(room, GAME);
}

int CRoomManager::GetRoomMemberCount(const shared_ptr<CPlayer>& innerMember)
{
	std::shared_ptr<CRoom> room = GetRoom(innerMember);
	if (room == nullptr) return 0;
	return GetRoomMemberCount(room);
}

int CRoomManager::GetRoomMemberCount(const std::shared_ptr<CRoom>& room)
{
	return room->GetPlayerCount();
}

void CRoomManager::OutRoom(const shared_ptr<CPlayer>& innerMember)
{
	std::shared_ptr<CRoom> room = GetRoom(innerMember);
	// 회원이 접속중인 모든 룸을 조회
	while (room) {
		int count = GetRoomMemberCount(room);
		// 혼자 있는 방은 삭제
		if(count == 1) DeleteRoom(room->GetRoomNumber());
		// 같이 있다면 null로 비워준다.
		else {
			mt_roomManager.lock();
			for (int i = 0; i < MAX_PLAYER; ++i) {
				auto player_i = room->GetPlayer(i);
				if (player_i == innerMember) {
					room->DeletePlayer(i, true);
					break;
				}
			}
			mt_roomManager.unlock();

			// 나머지 인원들에게 변경 사항을 알려준다.
			room->SendRoomInfoToAllMember();
			CheckAndRestateRoom(room);

			// 혹시라도 룸이 비어버렸다면 다시한번 삭제처리를 한다.
			if (GetRoomMemberCount(room) == 0) DeleteRoom(room->GetRoomNumber());
		}
		room = GetRoom(innerMember);
	}
}

bool CRoomManager::MoveRoom(const shared_ptr<CPlayer>& innerMember, const shared_ptr<CPlayer>& outterMember, int& outSlotNumber)
{
	// 룸이동 성공?
	if (_MoveRoomProc(innerMember, outterMember, outSlotNumber)) {
		std::shared_ptr<CRoom> innerRoom = GetRoom(innerMember);
		if (innerRoom) innerRoom->SendRoomInfoToAllMember();
		WriteLog(ELogLevel::Warning, "Success to move.\n");
		return true;
	}
	// 룸이동 실패?
	else {
		char allBuf[sizeof(EMessageType) + 30], buf[30], msg[] = "방 이동에 실패하였습니다.";
		int stringLen = (int)strlen(msg);
		StringSerialize(buf, msg, stringLen);
		int len = SerializeWithEnum(S_Lobby_InviteFriend_Failed, buf, stringLen, allBuf);
		if (outterMember) CServerNetworkSystem::GetInstance()->Send(outterMember, allBuf, len, true);
		if (innerMember) CServerNetworkSystem::GetInstance()->Send(innerMember, allBuf, len, true);
		WriteLog(ELogLevel::Warning, "Failed to move.\n");
		return false;
	}
}