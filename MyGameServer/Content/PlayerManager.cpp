#include "PlayerManager.h"
#include "NetworkModule/Log.h"
#include "Server/ServerNetworkSystem.h"
#include <algorithm>
using namespace std;
typedef lock_guard<std::recursive_mutex> Lock;

CPlayerManager* CPlayerManager::instance = nullptr;

CPlayerManager * CPlayerManager::GetInstance()
{
	if (!instance) instance = new CPlayerManager();
	return instance;
}

CPlayerManager::CPlayerManager() {}

shared_ptr<CPlayer> CPlayerManager::GetPlayerById(const UINT64& steamID)
{
	shared_ptr<CPlayer> retval;
	
	Lock locker(playersMutex);
	for (auto i : players) {
		if (i->steamID == steamID) {
			retval = i;
			break;
		}
	}
	return retval;
}

shared_ptr<CPlayer> CPlayerManager::GetPlayerBySocket(CSocket* targetSocket)
{
	shared_ptr<CPlayer> retval;
	Lock locker(playersMutex);
	for (auto i : players) {
		if (i->socket == targetSocket) {
			retval = i;
			break;
		}
	}
	return retval;
}

std::shared_ptr<CPlayer> CPlayerManager::GetPlayerBySocket(SOCKET targetSocket)
{
	shared_ptr<CPlayer> retval;
	Lock locker(playersMutex);
	for (auto i : players) {
		if (i->socket->GetSocket() == targetSocket) {
			retval = i;
			break;
		}
	}
	return retval;
}

shared_ptr<CPlayer> CPlayerManager::GetPlayerByNum(const int& num)
{
	shared_ptr<CPlayer> retval;
	Lock locker(playersMutex);
	if (num < (int)players.size()) {
		retval = players[num];
	}
	return retval;
}

shared_ptr<CPlayer> CPlayerManager::GetPlayerByUDPAddr(const FSocketAddrIn & addr)
{
	shared_ptr<CPlayer> retval;
	Lock locker(playersMutex);
	for (auto i : players) {
		if (i->udpAddr &&
			i->udpAddr->sin_addr.s_addr == addr.sin_addr.s_addr &&
			i->udpAddr->sin_port == addr.sin_port) {
			retval = i;
			break;
		}
	}
	return retval;
}

shared_ptr<CPlayer> CPlayerManager::GetNoUDPAddrPlayerByTCPAddr(const FSocketAddrIn& addr)
{
	shared_ptr<CPlayer> retval;
	Lock locker(playersMutex);
	for (auto i : players) {
		if (i->udpAddr == nullptr && i->addr.sin_addr.s_addr == addr.sin_addr.s_addr) {
			retval = i;
			break;
		}
	}
	return retval;
}
shared_ptr<CPlayer> CPlayerManager::GetPlayerByTCPAddr(const FSocketAddrIn & addr)
{
	shared_ptr<CPlayer> retval = nullptr;
	Lock locker(playersMutex);
	for (auto i : players) {
		if (i->addr.sin_addr.s_addr == addr.sin_addr.s_addr) {
			retval = i;
			break;
		}
	}
	return retval;
}

void CPlayerManager::AddPlayer(const std::shared_ptr<CPlayer>& newPlayer)
{	
	Lock locker(playersMutex);
	players.push_back(newPlayer);
}

shared_ptr<CPlayer> CPlayerManager::EditPlayerIDBySocket(CSocket* targetSock, const UINT64& steamID)
{
	shared_ptr<CPlayer> retval;
	Lock locker(playersMutex);
	for (auto i : players) {
		if (i->socket == targetSock) {
			retval = i;
			retval->steamID = steamID;
			break;
		}
	}
	return retval;
}

int CPlayerManager::GetPlayerCount()
{
	return static_cast<int>(players.size());
}

void CPlayerManager::RemovePlayerById(const UINT64& playerID)
{
	shared_ptr<CPlayer> removeNeedPoint;
	Lock locker(playersMutex);
	auto i = std::begin(players);

	while (i != std::end(players)) {
		// Do some stuff
		if ((*i)->steamID == playerID)
		{
			removeNeedPoint = *i;
			players.erase(i);
			break;
		}
		else
			++i;
	}
}

void CPlayerManager::RemovePlayerBySocket(CSocket* playerSocket)
{
	shared_ptr<CPlayer> removeNeedPoint = nullptr;
	Lock locker(playersMutex);
	auto i = std::begin(players);

	while (i != std::end(players)) {
		// Do some stuff
		if ((*i)->socket == playerSocket)
		{
			removeNeedPoint = *i;
			players.erase(i);
			break;
		}
		else
			++i;
	}
}

void CPlayerManager::RemovePlayer(const std::shared_ptr<CPlayer>& player, bool withLock)
{
	shared_ptr<CPlayer> removeNeedPoint;
	
	if (withLock) playersMutex.lock();
	auto i = std::begin(players);

	while (i != std::end(players)) {
		// Do some stuff
		if (*i == player)
		{
			removeNeedPoint = *i;
			players.erase(i);
			break;
		}
		else
			++i;
	}
	if (withLock) playersMutex.unlock();
}

void CPlayerManager::AbsorbPlayer(std::shared_ptr<CPlayer> from_new, std::shared_ptr<CPlayer> to_old)
{
	if (from_new.get() == to_old.get()) return;
	Lock locker(playersMutex);
	to_old->steamID = from_new->steamID;
	to_old->addr = from_new->addr;
	to_old->socket = from_new->socket;
	to_old->lastPingTime = from_new->lastPingTime;
	to_old->lastPongTime = from_new->lastPongTime;
	to_old->state = EGameState::GAME;

	// 흡수전 플레이어를 삭제시킨다.
	RemovePlayer(from_new, false);
}

CPlayerManager::~CPlayerManager()
{
	if (playersMutex.try_lock()) {
		players.clear();
		playersMutex.unlock();
	}
	else {
		std::cout << "~CPlayerManager : Can't clear the vector.\n";
	}
}
