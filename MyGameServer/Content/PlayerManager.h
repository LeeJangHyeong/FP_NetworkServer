#pragma once
/*
	플레이어의 정보를 관할하는 클래스
*/

#include "MyGameServer.h"
#include "NetworkModule/NetworkData.h"
#include "DataType/Player.h"
#include <vector>
#include <mutex>
#include <iostream>
#include <memory>

using std::vector;
struct FOverlapped;

class CPlayerManager
{
public:
	static CPlayerManager* GetInstance();

	// nullptr when player not exist
	std::shared_ptr<CPlayer> GetPlayerById(const UINT64& steamID);
	std::shared_ptr<CPlayer> GetPlayerBySocket(CSocket* targetSocket);
	std::shared_ptr<CPlayer> GetPlayerBySocket(SOCKET targetSocket);
	std::shared_ptr<CPlayer> GetPlayerByNum(const int& num);
	std::shared_ptr<CPlayer> GetPlayerByUDPAddr(const FSocketAddrIn& addr);
	std::shared_ptr<CPlayer> GetPlayerByTCPAddr(const FSocketAddrIn& addr);
	std::shared_ptr<CPlayer> GetNoUDPAddrPlayerByTCPAddr(const FSocketAddrIn& addr);

	void AddPlayer(const std::shared_ptr<CPlayer>& newPlayerInfo);
	std::shared_ptr<CPlayer> EditPlayerIDBySocket(CSocket* targetSocket, const UINT64& steamID);
	int GetPlayerCount();
	void RemovePlayerById(const UINT64& playerID);
	void RemovePlayerBySocket(CSocket* playerSocket);
	void RemovePlayer(const std::shared_ptr<CPlayer>& player, bool withLock = true);
	void AbsorbPlayer(std::shared_ptr<CPlayer> from_new, std::shared_ptr<CPlayer> to_old);
	~CPlayerManager();

private:
	CPlayerManager();
	std::recursive_mutex playersMutex;
	vector<std::shared_ptr<CPlayer>> players;
	static CPlayerManager* instance;
};

